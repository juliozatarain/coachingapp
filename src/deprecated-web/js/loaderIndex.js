//Librerías
document.write('<script type="text/javascript" src="js/libs/cordova-2.7.0.js"></script>');
document.write('<script type="text/javascript" src="js/libs/zepto.min.js"></script>');
document.write('<script type="text/javascript" src="js/libs/touch.js"></script>');
document.write('<script type="text/javascript" src="js/libs/mobiscroll.zepto.js"></script>');
document.write('<script type="text/javascript" src="js/libs/mobiscroll.core.js"></script>');
document.write('<script type="text/javascript" src="js/libs/mobiscroll.datetime.js"></script>');
document.write('<script type="text/javascript" src="js/libs/mobiscroll.select.js"></script>');
document.write('<script type="text/javascript" src="js/libs/mobiscroll.i18n.es.js"></script>');

document.write('<script type="text/javascript" src="js/libs/d3.min.mod.js"></script>');
document.write('<script type="text/javascript" src="js/libs/iscroll-lite.js"></script>');
document.write('<script type="text/javascript" src="js/libs/jquery.magnific-popup.min.js"></script>');


//Helpers
document.write('<script type="text/javascript" src="js/helpers/Validacion.js"></script>');
document.write('<script type="text/javascript" src="js/helpers/Notificacion.js"></script>');
document.write('<script type="text/javascript" src="js/helpers/Fecha.js"></script>');


//Modelos
document.write('<script type="text/javascript" src="js/models/Database.js"></script>');
document.write('<script type="text/javascript" src="js/models/Usuario.js"></script>');
document.write('<script type="text/javascript" src="js/models/HistorialPeso.js"></script>');
document.write('<script type="text/javascript" src="js/models/ConfiguracionUsuario.js"></script>');
document.write('<script type="text/javascript" src="js/models/CategoriaAlimento.js"></script>');
document.write('<script type="text/javascript" src="js/models/Alimento.js"></script>');
document.write('<script type="text/javascript" src="js/models/Tip.js"></script>');



//Vistas
document.write('<script type="text/javascript" src="js/views/informacion/ListaAlimentos.js"></script>');
document.write('<script type="text/javascript" src="js/views/informacion/ListaCategorias.js"></script>');
document.write('<script type="text/javascript" src="js/views/informacion/ListaTips.js"></script>');
document.write('<script type="text/javascript" src="js/views/informacion/TipSencillo.js"></script>');


document.write('<script type="text/javascript" src="js/views/progresopeso/RegistroMeta.js"></script>');
document.write('<script type="text/javascript" src="js/views/progresopeso/RegistroPeso.js"></script>');

document.write('<script type="text/javascript" src="js/views/usuario/FormaActualizaUsuario.js"></script>');
document.write('<script type="text/javascript" src="js/views/usuario/FormaRegistroUsuarioView.js"></script>');
document.write('<script type="text/javascript" src="js/views/usuario/Menu.js"></script>');

document.write('<script type="text/javascript" src="js/views/graficas/GraficaIMC.js"></script>');
document.write('<script type="text/javascript" src="js/views/graficas/GraficaPeso.js"></script>');


//Controladores
document.write('<script type="text/javascript" src="js/controllers/DatosUsuario.js"></script>');
document.write('<script type="text/javascript" src="js/controllers/OperacionesPeso.js"></script>');
document.write('<script type="text/javascript" src="js/controllers/Contenidos.js"></script>');




//Se espera a que cargue cordova
window.addEventListener('load',function(){
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
        document.addEventListener("deviceready", onDeviceReady, false);
    }
    else
    {
        $(document).ready(function()
        {
            onDeviceReady();
        });
    }
});
    
function onDeviceReady() 
{
    
    //cuando se detecta el click fuera dle teclado se hace blur para esconder el teclado
    $('body').on("tap",  function escuchadorDeTouch(e){
        if(e.target.tagName != 'INPUT' && e.target.tagName != 'SELECT')
        {
            document.activeElement.blur();
        }
    });


    var datosUsuario = new DatosUsuario();
    datosUsuario.index();
    
}

var duracionFadeIn = 500;
function animaVista(animacionTerminada)
{
     $("body").animate({
          opacity: 1.0
        }, duracionFadeIn, 'ease-in', animacionTerminada());


}