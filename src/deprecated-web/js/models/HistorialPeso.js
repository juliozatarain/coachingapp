/**
 * Crea una instancia de HistorialPeso
 * @constructor
 * @this {HistorialPeso}
 */
function HistorialPeso()
{
    /** @private */this.id = null;
    /** @private */this.usuario_id = null;
    /** @private */this.fecha_registro = null;
    /** @private */this.peso = null;
    /** @private */this.database  = new Database();
}

HistorialPeso.prototype =
{
    /**
     * Obtiene todos los registros de historiales_peso
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */          
    getRecords: function(controllerCallback)
    {
        this.database.transaction("SELECT * FROM historiales_peso;", [], getRecordsCallback);
        function getRecordsCallback(tx,results)
        {
            var coleccion = new Array();
            for(var i = 0; i < results.rows.length; i++)
            {
                var historialpeso = new HistorialPeso();
                 
                historialpeso.id = results.rows.item(i).id;
                historialpeso.usuario_id = results.rows.item(i).usuario_id;
                historialpeso.fecha_registro = results.rows.item(i).fecha_registro;
                historialpeso.peso = results.rows.item(i).peso; 
                coleccion.push(historialpeso);
            }
            controllerCallback(coleccion);
        }
    },
    /**
     * Guarda los datos del objeto en el registro correspondiente de la tabla historiales_peso
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */     
     save : function(controllerCallback)
    {
        var obj = this;
        if(this.id != null)
        {
            this.database.transaction("UPDATE historiales_peso SET usuario_id = ?, fecha_registro = ?, peso = ?  WHERE id = ?", [this.usuario_id, this.fecha_registro, this.peso, this.id], saveCallback);
        }
        else
        {
            this.database.transaction("INSERT INTO historiales_peso(usuario_id, fecha_registro, peso) VALUES (?,?,?) ", [this.usuario_id, this.fecha_registro, this.peso], saveCallback);
        }
        function saveCallback(tx, results)
        {  
            if(obj.id == null)
            {
                obj.id = results.insertId;
            }
            controllerCallback();
        }
    },
    /**
     * Revisa que la fecha no exista en la tabla de historiales peso
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */     
     validaFecha: function (controllerCallback)
    {
        var obj = this;
        this.database.transaction("SELECT * FROM historiales_peso WHERE usuario_id = 1 ORDER by substr(fecha_registro,1,4)||substr(fecha_registro,5,6)||substr(fecha_registro,7) DESC LIMIT 1;",[], validaFechaModelCallback);
        function validaFechaModelCallback(tx, results)
        {
            var fecha_registro = results.rows.item(0).fecha_registro;
            if(obj.fecha_registro == fecha_registro)
            {
                obj.id = results.rows.item(0).id;
                controllerCallback(false);

            }
            else
            {
                controllerCallback(true);
            }
            
        }
    },
    obtenerReciente: function (controllerCallback)
    {
        var obj = this;
        this.database.transaction("SELECT * FROM historiales_peso WHERE usuario_id = 1 ORDER by substr(fecha_registro,1,4)||substr(fecha_registro,5,6)||substr(fecha_registro,7) DESC LIMIT 1;",[], obtenerRecienteModelCallback);
        function obtenerRecienteModelCallback(tx, results)
        {
            obj.id = results.rows.item(0).id;
            obj.usuario_id = results.rows.item(0).usuario_id;
            obj.fecha_registro = results.rows.item(0).fecha_registro;
            obj.peso = results.rows.item(0).peso;
            if(controllerCallback)
            {
                controllerCallback();
            }
         
        }
    },
    obtenerPrimero : function(controllerCallback)
    {
        var obj = this;
        this.database.transaction("SELECT * FROM historiales_peso WHERE usuario_id = 1 ORDER by substr(fecha_registro,1,4)||substr(fecha_registro,5,6)||substr(fecha_registro,7) LIMIT 1;",[], obtenerPrimeroModelCallback);
        function obtenerPrimeroModelCallback(tx, results)
        {
            obj.id = results.rows.item(0).id;
            obj.usuario_id = results.rows.item(0).usuario_id;
            obj.fecha_registro = results.rows.item(0).fecha_registro;
            obj.peso = results.rows.item(0).peso;
            if(controllerCallback)
            {
                
                controllerCallback();
            }
                 
                 }
    }
}