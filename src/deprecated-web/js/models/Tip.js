/**
 * Crea una instancia de Tip
 * @constructor
 * @this {Tip}
 */
function Tip()
{
    /** @private */this.id = null;
    /** @private */this.titulo = null;
    /** @private */this.texto = null;
    /** @private */this.database  = new Database();    
}

Tip.prototype = 
{
    /**
     * Obtiene todos los registros de tips
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */      
    getRecords: function(controllerCallback)
    {
        this.database.transaction("SELECT * FROM tips;", [], getRecordsCallback);
        function getRecordsCallback(tx,results)
        {
            var coleccion = new Array();
            for(var i = 0; i < results.rows.length; i++)
            {
                var tip = new Tip(); 
                tip.id = results.rows.item(i).id;
                tip.titulo = results.rows.item(i).titulo;
                coleccion.push(tip);
            }
            controllerCallback(coleccion);
        }
    },
    /**
     * Obtiene el registro de tips con el id idParam
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */     
     find: function(idParam, controllerCallback)
    {
        var obj = this;
        this.database.transaction("SELECT * FROM tips WHERE id = ?", [idParam], getRecordCallback);
        function getRecordCallback(tx, results)
        {
            obj.id = results.rows.item(0).id;
            obj.titulo = results.rows.item(0).titulo;
            obj.nombreArchivo = results.rows.item(0).nombreArchivo;
            obj.texto = results.rows.item(0).texto;
            controllerCallback();
        }
    }
}