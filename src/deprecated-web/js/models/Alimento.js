/**
 * Crea una instancia de Alimento
 * @constructor
 * @this {Alimento}
 */
function Alimento()
{
/** @private */this.id = null;
/** @private */this.nombre = null;
/** @private */this.kcal = null;
/** @private */this.database  = new Database();    
}

Alimento.prototype = 
{
    /**
     * Obtiene los registros de alimento pertenecientes a la categoria con id idParama 
     * @param {int} idParam valor númerico para buscar el registro
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */       
    find: function(idParam, controllerCallback)
    {
        this.database.transaction("SELECT * FROM alimentos WHERE categoria_id = ?", [idParam], getRecordCallback);

        function getRecordCallback(tx, results)
        {            
            var coleccion = new Array();
            for(var i = 0; i < results.rows.length; i++)
            {
                var alimento = new Alimento(); 
                alimento.id = results.rows.item(i).id;
                alimento.nombre = results.rows.item(i).nombre;
                alimento.kcal = results.rows.item(i).kcal;
                coleccion.push(alimento);
            }
            controllerCallback(coleccion);
        }
    }
}