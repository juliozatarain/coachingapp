/**
 * Crea una instancia de ConfiguracionUsuario
 * @constructor
 * @this {ConfiguracionUsuario}
 */
function ConfiguracionUsuario()
{
/** @private */this.id = null;
/** @private */this.usuario_id = null;
/** @private */this.fecha_registro = null;
/** @private */this.peso_ideal = null;
/** @private */this.factor_reduccion = null;
/** @private */this.database = new Database();
}

ConfiguracionUsuario.prototype =
{
    /**
     * Guarda los datos del objeto en el registro correspondiente de la tabla configuraciones_usuario
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */  
    save : function(controllerCallback)
    {
        var obj = this;

        if(this.id != null)
        {
            this.database.transaction("UPDATE configuraciones_usuario SET usuario_id = ?, fecha_registro = ?, fecha_objetivo = ?, peso_ideal = ?, meta_activa = ?, factor_reduccion = ?, fecha_notificacion = ?  WHERE id = ?", [this.usuario_id, this.fecha_registro, this.fecha_objetivo, this.peso_ideal, this.meta_activa, this.factor_reduccion, this.fecha_notificacion, this.id], saveCallback);
        }
        else
        {
            this.database.transaction("INSERT INTO configuraciones_usuario(usuario_id, fecha_registro, fecha_objetivo, peso_ideal, meta_activa, factor_reduccion, fecha_notificacion) VALUES (?,?,?,?,?,?,?) ", [this.usuario_id, this.fecha_registro, this.fecha_objetivo, this.peso_ideal, this.meta_activa, this.factor_reduccion, this. fecha_notificacion], saveCallback);
        }
        function saveCallback(tx, results)
        {  

            if(obj.id == null)
            {
                obj.id = results.insertId;
            }
           if(controllerCallback)
                controllerCallback();
            
        }
    },
    /**
     * Obtiene la configuracion de usuario más reciente
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */      
     obtenerReciente: function (controllerCallback)
    {
        var obj = this;
        this.database.transaction("SELECT * FROM configuraciones_usuario WHERE usuario_id = 1 ORDER BY id DESC LIMIT 1;",[], obtenerRecienteModelCallback);
        function obtenerRecienteModelCallback(tx, results)
        {
            if(results.rows.length > 0)
            {
                obj.id = results.rows.item(0).id;
                obj.usuario_id = results.rows.item(0).usuario_id;
                obj.fecha_registro = results.rows.item(0).fecha_registro;
                obj.fecha_objetivo = results.rows.item(0).fecha_objetivo;
                obj.peso_ideal = results.rows.item(0).peso_ideal;
                obj.meta_activa = results.rows.item(0).meta_activa;
                obj.factor_reduccion = results.rows.item(0).factor_reduccion;
                obj.fecha_notificacion = results.rows.item(0).fecha_notificacion;
            }
            controllerCallback();
        }
    }
}
