/**
 * Crea una instancia de CategoriaAlimento
 * @constructor
 * @this {CategoriaAlimento}
 */
function CategoriaAlimento()
{
/** @private */this.id = null;
/** @private */this.nombre = null;
/** @private */this.database  = new Database();    
}

CategoriaAlimento.prototype = 
{

    /**
     * Obtiene todos los registros de categoria de alimento
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */      
     getRecords: function(controllerCallback)
    {
        this.database.transaction("SELECT * FROM categorias_alimento;", [], getRecordsCallback);
        function getRecordsCallback(tx,results)
        {
            var coleccion = new Array();
            for(var i = 0; i < results.rows.length; i++)
            {
                var categoria = new CategoriaAlimento(); 
                categoria.id = results.rows.item(i).id;
                categoria.nombre = results.rows.item(i).nombre;
                coleccion.push(categoria);
            }
            controllerCallback(coleccion);
        }
    }
}