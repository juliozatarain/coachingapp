var da = new Database();

/**
 * Crea una instancia de Database
 * @constructor
 * @this {Database}
 */
function Database()
{
/** @private */this.databaseNameInst = "Redustat";
/** @private */this.databaseVersionInst = "1.0";
/** @private */this.databaseDisplayName = "Redustat";
/** @private */this.databaseSize = 200000;
/** @private */this.db = window.openDatabase(this.databaseNameInst, this.databaseVersionInst, this.databaseDisplayName, this.databaseSize);
}

Database.prototype =
{
    /**
     * Guarda los datos del objeto en el registro correspondiente de la tabla configuraciones_usuario
     * @param {function} query sentencia a ejecutar
     * @param {Array} placeHolderArr arreglo que almacena los resultados de la sentencia 
     * @param {function} successCallback funcion que se ejecuta al terminar de ejecutar el query
     * @return {void} 
     */    
     transaction: function(queryParam, placeHolderArrParam, successCallback)
    {
        this.db.transaction(function(tx) { 
            tx.executeSql(queryParam, placeHolderArrParam, successCallback, this.errorDB)
        });
    },
    /**
     * Función que se ejecuta al ocurrir un error en la base de datos
     * @param {error} errorParam objeto error que contiene información sobre el error que ocurrio
     * @return {void} 
     */      
     errorDB: function(errorParam)
    {
        console.log("ERROR AL EJECUTAR QUERY" + errorParam.code);
        alert("Error al ejecutar query: "+errorParam.code);
    },
    /**
     * Función que se encarga de construir la base de datos
     * @return {void} 
     */        
     initTables: function()
    {
        this.db = window.openDatabase(this.databaseNameInst, this.databaseVersionInst, this.databaseDisplayName,  this.databaseSize);
        this.db.transaction(function(tx){
            
            /*tx.executeSql('DROP TABLE IF EXISTS usuarios'); 
            tx.executeSql('DROP TABLE IF EXISTS historiales_peso');
            tx.executeSql('DROP TABLE IF EXISTS configuraciones_usuario');*/
            tx.executeSql('DROP TABLE IF EXISTS tips');
            tx.executeSql('DROP TABLE IF EXISTS alimentos');
            tx.executeSql('DROP TABLE IF EXISTS categorias_alimento');
           
            tx.executeSql('CREATE TABLE IF NOT EXISTS usuarios (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nombre TEXT, apellido TEXT, peso REAL, estatura REAL, fecha_nacimiento NUMERIC, genero INTEGER)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS historiales_peso (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, usuario_id INTEGER, fecha_registro NUMERIC, peso REAL)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS configuraciones_usuario (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, usuario_id INTEGER, fecha_registro NUMERIC, fecha_objetivo NUMERIC, peso_ideal REAL, meta_activa BOOLEAN, factor_reduccion REAL, fecha_notificacion NUMERIC)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS tips (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,titulo TEXT, nombreArchivo TEXT,texto TEXT)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS alimentos (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, categoria_id INTEGER, nombre TEXT, kcal INTEGER)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS categorias_alimento (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nombre TEXT)');

            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Compras","Compras.jpg","Programa un horario para hacer las compras de la semana y preparar comidas y refrigerios. Existe una infinidad de recetas prácticas y económicas que te harán ahorrar dinero y mejorar tu salud.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Productos light","Productos-light.jpg","Prefiere siempre productos lácteos descremados, bajos en grasa o light.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Empanizado","Empanizado.jpg","Empaniza la carne con una mezcla de salvado y germen de trigo para agregar más fibra.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Frutas y Verduras","Frutas-y-Verduras.jpg","Incluye siempre en tus comidas una guarnición de verduras al vapor o una ensalada de frutas de temporada. Combina diferentes colores para asegurar la ingesta de diferentes nutrientes.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Cereales","Cereales.jpg","Los cereales integrales (arroz, harina, pan) aportan más vitaminas, minerales y fibras que los refinados.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Entre comidas","Entre-comidas.jpg","Ten al alcance dos o tres tipos de fruta para comerlas entre comidas, y también para preparar jugos, ensaladas y otros platillos.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Carnes","Carnes.jpg","Consume mayor cantidad de pescado y menor cantidad de carnes rojas como res y puerco. En general, se consideran carnes rojas las provenientes de los mamíferos.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Nutrientes vegetales","Nutrientes-vegetales.jpg","Conserva todos tus vegetales en el refrigerador para evitar que pierdan sus nutrientes.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Sandwich","Sandwich.jpg","Prepara tus sándwiches con pan de cereales integrales y usa ingredientes saludables como aceitunas, ensalada de atún, queso panela o una pequeña porción de carne.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Ensaladas ","Ensaladas.jpg","Agrega nueces, almendras, avellanas y pepitas a tus ensaladas para enriquecerlas y hacerlas más “llenadoras”. También puedes agregar pasta, arroz, pan tostado sin aceite, queso, pescado, jamón o huevo duro.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Postres","Postres.jpg","Incluye siempre bebidas y postres en tus comidas de preferencia sin azúcar.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Bebidas","Bebidas.jpg","En el supermercado puedes encontrar aguas naturales saborizadas bajas en azúcar. Son una gran opción de bebida para tus comidas.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Galletas","Galletas.jpg","En lugar de galletas saladas, consume galletas integrales, que son bajas en sodio.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Sazón","Sazón.jpg","Evita el exceso de grasa, azúcar y sal. Utiliza siempre la mínima cantidad para darle sabor a tus platillos.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Piel de pollo","Piel-de-pollo.jpg","Retira la piel del pollo antes de cocinarlo.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Aceites vegetales","Aceites-vegetales.jpg","Consume aceite vegetal en spray y cocina en recipientes antiadherentes para minimizar el uso de grasas.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Sal","Sal.jpg","Reemplaza la sal con hierbas y especias para dar sabor a los alimentos, o con sal de mar. Ajo, perejil, romero y laurel son excelentes opciones.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Caldos y sopas","Caldos-y-sopas.jpg","Si preparas tus caldos y sopas un día antes de consumirlos, podrás retirar fácilmente la grasa solidificada antes de calentarlos.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Hamburguesas","Hamburguesas.jpg","Prepara la carne de tus hamburguesas caseras con una buena cantidad de verduras ralladas y complementa con lechuga, jitomate, cebolla, pepinos y zanahoria.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Condimentos","Condimentos.jpg","Realza el sabor de la comida con ajo, jengibre, mostaza, cebolla y jugo de frutas cítricas.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Frutas y verduras","Frutas-y-verduras.jpg","Hierve las verduras el mínimo tiempo posible, con poco agua, y sírvelas enseguida. En lo posible, consúmelas crudas.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Horno microondas","Horno-microondas.jpg","Trata de preparar tus comidas en el horno de microondas. Es más práctico y evita el uso de grasas.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Utensilios","Utensilios.jpg","Procura que tus utensilios sean de vidrio, acero inoxidable o porcelana.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Cocción","Coccion.jpg","Trata de no sobrecalentar, tostar ni requemar los alimentos.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Alimentos al vapor","Alimentos-al-vapor.jpg","Es mejor preparar los alimentos al vapor o a la plancha, y no fritos o rebozados")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Aceite","Aceite.jpg","Utiliza la mínima cantidad de aceite para freír y coloca papel absorbente debajo de los alimentos para remojarlos durante unos minutos antes de servir.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Carnes","Carnes.jpg","Aprovecha la rejilla del horno para preparar carnes a la parrilla: condiméntalas con especias y hornea hasta que queden cocidas. Puedes colocar un recipiente hondo debajo para interceptar la grasa y desecharla.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Vinagre","Vinagre.jpg","Puedes usar limón en lugar de vinagre para aderezar tus ensaladas y también para marinar.")');
            tx.executeSql('INSERT INTO tips(titulo,nombreArchivo,texto) VALUES("Huevo","Huevo.jpg","Cuando consumas huevo, en lo posible intenta retirar las yemas para reducir el colesterol.")');
            
            tx.executeSql('INSERT INTO categorias_alimento (nombre) VALUES ("Verduras y hortalizas")');
            tx.executeSql('INSERT INTO categorias_alimento (nombre) VALUES ("Frutas")');
            tx.executeSql('INSERT INTO categorias_alimento (nombre) VALUES ("Frutos secos")');
            tx.executeSql('INSERT INTO categorias_alimento (nombre) VALUES ("Cereales y pasta")');
            tx.executeSql('INSERT INTO categorias_alimento (nombre) VALUES ("Lácteos")');
            tx.executeSql('INSERT INTO categorias_alimento (nombre) VALUES ("Carnes")');
            tx.executeSql('INSERT INTO categorias_alimento (nombre) VALUES ("Pescados")');
            tx.executeSql('INSERT INTO categorias_alimento (nombre) VALUES ("Otros")');

            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Ajos","1","138")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Apio","1","19")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Cebolla","1","46")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Espinacas","1","31")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Lechuga","1","17")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Papa cocida","1","85")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pepino","1","54")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pimiento","1","21")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Tomate","1","21")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Zanahoria","1","41")');

            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Aguacate","2","166")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Cerezas","2","76")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Ciruelas","2","43")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Coco","2","645")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Fresas","2","35")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Higos","2","79")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Limón","2","38")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Manzana","2","51")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Melón","2","30")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Naranja","2","43")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Papaya","2","44")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Peras","2","60")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Piña","2","50")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Plátano","2","89")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Sandía","2","29")');

            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Almendras","3","619")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Avellanas","3","674")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Cacahuate","3","636")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Nueces","3","659")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pistache","3","580")');

            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Arroz blanco","4","353")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Arroz integral","4","349")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Avena","4","366")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pan de trigo integral","4","238")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pan de trigo blanco","4","254")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pasta ","4","367")');

            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Leche de vaca entera","5","67")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Leche de vaca semidescremada","5","48")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Queso manchego","5","375")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Queso parmesano","5","392")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Yogur natural","5","61")');


            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Chuleta de cerdo","6","329")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Lomo de cerdo","6","207")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pavo","6","222")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pollo","6","84")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Bistec","6","180")');

            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Atún","7","224")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Bacalao seco","7","321")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Camarones","7","95")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Pulpo","7","56")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Salmón","7","171")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Sardina","7","150")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Trucha","7","93")');

            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Huevo entero","8","161")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Azúcar","8","379")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Miel","8","299")');
            tx.executeSql('INSERT INTO alimentos (nombre,categoria_id,kcal) VALUES ("Aceitunas","8","148")');
        }, this.errorDB);
       
    }
    
}