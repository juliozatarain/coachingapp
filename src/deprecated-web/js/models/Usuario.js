/**
 * Crea una instancia de Usuario
 * @constructor
 * @this {Usuario}
 */
function Usuario()
{
    /** @private */this.id = null;
    /** @private */this.nombre = null;
    /** @private */this.apellido = null;
    /** @private */this.peso = null;
    /** @private */this.estatura = null;
    /** @private */this.fechaNacimiento = null;
    /** @private */this.genero = null;
    
    this.database  = new Database();    
}

Usuario.prototype = 
{
    /**
     * Obtiene todos los registros de usuarios
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */     
     getRecords: function(controllerCallback)
    {
        this.database.transaction("SELECT * FROM usuarios;", [], getRecordsCallback);
        function getRecordsCallback(tx,results)
        {
            var coleccion = new Array();
            for(var i = 0; i < results.rows.length; i++)
            {
                var usuario = new Usuario();
                 
                usuario.id = results.rows.item(i).id;
                usuario.nombre = results.rows.item(i).nombre;
                usuario.apellido = results.rows.item(i).apellido;
                usuario.peso = results.rows.item(i).peso;
                usuario.estatura = results.rows.item(i).estatura;
                usuario.fechaNacimiento = results.rows.item(i).fecha_nacimiento;
                usuario.genero = results.rows.item(i).genero;
                coleccion.push(usuario);
            }
            controllerCallback(coleccion);
        }
    },
    /**
     * Obtiene el registro de usuarios con el id idParam
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */    
    find: function(idParam, controllerCallback)
    {
        var obj = this;
        this.database.transaction("SELECT * FROM usuarios WHERE id = ?", [idParam], getRecordCallback);
        function getRecordCallback(tx, results)
        {
            
            obj.id = results.rows.item(0).id;
            obj.nombre = results.rows.item(0).nombre;
            obj.apellido = results.rows.item(0).apellido;
            obj.peso = results.rows.item(0).peso;
            obj.estatura = results.rows.item(0).estatura;
            obj.fechaNacimiento = results.rows.item(0).fecha_nacimiento;
            obj.genero = results.rows.item(0).genero;
            controllerCallback();
        }
    },
    /**
     * Guarda los datos del objeto en el registro correspondiente de la tabla Usuario
     * @param {function} controllerCallback función a ejecutar cuando se obtengan los datos
     * @return {void} 
     */      
     save : function(controllerCallback)
    {
        var obj = this;
        if(this.id != null)
        {
            this.database.transaction("UPDATE usuarios SET nombre = ?, apellido = ?, peso = ?, estatura = ?, fecha_nacimiento = ?, genero = ?  WHERE id = ?", [this.nombre, this.apellido, this.peso, this.estatura, this.fechaNacimiento, this.genero, this.id], saveCallback);
        }
        else
        {
            this.database.transaction("INSERT INTO usuarios(nombre, apellido, peso, estatura, fecha_nacimiento, genero) VALUES (?, ?,?,?,?,?) ", [this.nombre, this.apellido, this.peso, this.estatura, this.fechaNacimiento, this.genero], saveCallback);
        }
        function saveCallback(tx, results)
        {  
            if(obj.id == null)
            {
                obj.id = results.insertId;
            }
            controllerCallback();
        }
        
    }
}