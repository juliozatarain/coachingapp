/**
 * Crea una instancia de OperacionesPeso
 * @constructor
 * @this {OperacionesPeso}
 */
function OperacionesPeso()
{
    
}

OperacionesPeso.prototype =
{
    /**
    * Obtiene los datos de historial peso, carga y muestra la vista de GraficaPeso
    * @return {void} 
    */    
    graficaPeso: function()
    {
        var pesometa = new ConfiguracionUsuario();
        
        pesometa.obtenerReciente(pesometaObtenerRecienteCallback);
       
        function pesometaObtenerRecienteCallback()
        {
            var historialpeso = new HistorialPeso();      
            historialpeso.getRecords(historialpesoGetRecordsCallback);
            function historialpesoGetRecordsCallback(coleccion)
            {
                var pesoReal = new Array();
                var pesoMeta = pesometa.peso_ideal;
                for(var i = 0; i < coleccion.length; i++)
                {
                    var elementoReal = {
                        fecha: (coleccion[i].fecha_registro).toString(),
                        peso: (coleccion[i].peso).toString() 
                    }
                    
                    pesoReal.push(elementoReal);
                }
                
                var data = {
                    pesoReal: pesoReal,
                    pesoMeta: pesoMeta 
                };
                    
                var view = new GraficaPeso(data);
                view.loadView();
            }
        }
            
    },
    /**
    * Obtiene los datos de historial peso, calcula los datos de imc, carga y muestra la vista de GraficaIMC
    * @return {void} 
    */ 
    graficaIMC : function()
    {
        var usuario = new Usuario();
        usuario.find(1, usuarioFindControllerCallback);
        function usuarioFindControllerCallback()
        {
            var estaturaMtCuadrados = Math.pow((parseInt(usuario.estatura)/100), 2);
            var pesometa = new ConfiguracionUsuario();
            pesometa.obtenerReciente(pesometaObtenerRecienteCallback);
            function pesometaObtenerRecienteCallback()
            {
                var historialpeso = new HistorialPeso();      
                historialpeso.getRecords(historialpesoGetRecordsCallback);
                function historialpesoGetRecordsCallback(coleccion)
                {
                    var imcReal = new Array();
                    var imcMeta = new Array();
                    for(var i = 0; i < coleccion.length; i++)
                    {
                        var fecha = coleccion[i].fecha_registro.toString();
                        
                        var elementoReal = {
                            fecha: fecha,
                            imc: (coleccion[i].peso / estaturaMtCuadrados).toString() 
                        }
                        
                        var elementoDeseado = {
                            fecha: fecha,
                            imc : (pesometa.peso_ideal / estaturaMtCuadrados).toString()
                        }
                        
                        imcReal.push(elementoReal);
                        imcMeta.push(elementoDeseado);
                    }
                    var data = {
                        imcReal: imcReal,
                        imcMeta: imcMeta 
                    };
                    
                    var view = new GraficaIMC(data);
                    view.loadView();
                }
            }
            
            
        }
    },
    /**
    * Carga y ejecuta la vista de RegistroPeso
    * @return {void} 
    */ 
    registroPeso : function()
    {
        var view = new RegistroPeso();
        view.loadView();
    },
    /**
    * Carga y ejecuta la vista de RegistroMeta
    * @return {void} 
    */     
    registroMeta: function()
    {
        var view = new RegistroMeta();
        view.loadView();
    },
    /**
    * Obtiene los datos de la forma de RegistroPeso y actualiza/inserta los datos nuevos en historialpeso en la base de datos
    * @return {void} 
    */         
    actualizaPeso: function()
    {
        var fechaHoy = new Date();
        var validador = new Validacion([
            [$('#peso'), '^\\d+(\\.(\\d)+)?$', "Peso tiene que ser número"], 
            [$('#peso'), '^\\d{1,3}(\\.\\d{1,3})?$', "Peso debe tener tres digitos en enteros y decimales"]
            ]);
        if(validador.validar())
        {
            var historialpeso = new HistorialPeso();
            historialpeso.usuario_id = 1;
            historialpeso.fecha_registro = obtenerFechaHoy();
            historialpeso.peso = $('#peso').val();
            historialpeso.validaFecha(validaFechaCallback);
            function validaFechaCallback(validoHoy)
            {
                if(!validoHoy)
                {
                    muestraConfirmacion("Ya tienes un peso registrado hoy. ¿Quieres sustituirlo con este nuevo?", sobreescribirCallback,  "Redustat", ["Cancelar","Aceptar"]);
                    function sobreescribirCallback(button)
                    {
                        if(button == 2)
                        {
                            historialpeso.peso = $('#peso').val();
                            guardaPeso();
                        }
                        
                    }
                    
                }
                else
                {
                    guardaPeso();
                }
                function guardaPeso()
                {
                    historialpeso.save(saveHistorialPesoCallback);

                    function saveHistorialPesoCallback()
                    {

                        var datosUsuarioController = new DatosUsuario();

                        var pesoActual = historialpeso.peso;
                        var configuracionUsuario = new ConfiguracionUsuario();
                        configuracionUsuario.obtenerReciente(obtenerRecienteConfiguracionCallback);

                        function obtenerRecienteConfiguracionCallback()
                        {

                            if(configuracionUsuario.meta_activa == "true")
                            {
                                    // ejecutamos save para guardar false en meta_activa del usuario
                                    if(Number(pesoActual) <= Number(configuracionUsuario.peso_ideal))
                                        {                                            
                                            configuracionUsuario.meta_activa=false;
                                        }

                                    configuracionUsuario.save(resetPesoidealConfiguracionUsuarioCallback)

                                    function resetPesoidealConfiguracionUsuarioCallback()
                                    {
                                            var mensajeNotificacion="";


                                        

                                            
                                        if(fechaHoy >= parseDate(dateToString(configuracionUsuario.fecha_objetivo)))
                                        {
                                            configuracionUsuario.meta_activa=false;


                                            if(Number(pesoActual) <= Number(configuracionUsuario.peso_ideal) && daydiff(fechaHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo))) <1)  
                                            {
                                                mensajeNotificacion="Felicidades, has alcanzado tu meta de peso a tiempo ¿Deseas establecer una nueva meta?";
                                            }
                                            else if(Number(pesoActual) <= Number(configuracionUsuario.peso_ideal))
                                            {
                                                if(Math.floor(daydiff(fechaHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo)))) > 1)
                                                {
                                                    mensajeNotificacion="Has alcanzado tu meta de peso "+Math.floor(daydiff(fechaHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo)))) +" días después de lo planeado. ¿Deseas establecer una nueva meta?" ;
                                                }
                                                else
                                                {
                                                    mensajeNotificacion="Has alcanzado tu meta de peso 1 día después de lo planeado. ¿Deseas establecer una nueva meta?" ;
                                                }
                                            }
                                            else 
                                            {
                                                mensajeNotificacion="No has alcanzado tu meta de peso ¿Deseas establecer una nueva meta?" ;
                                            }
                                            
                                            muestraConfirmacion(mensajeNotificacion,accionCambiarMetaCallback,"Redustat", "Cancelar,Aceptar");

                                        }
                                        else
                                        {

                                            if(Number(pesoActual) <= Number(configuracionUsuario.peso_ideal)) 
                                            {
                                                if(Math.floor(daydiff(fechaHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo))))>1)
                                                {
                                                    mensajeNotificacion="Felicidades, has alcanzado tu meta de peso "+Math.floor(daydiff(fechaHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo)))) +" días antes de lo planeado! ¿Deseas establecer una nueva meta?" ;

                                                }
                                                else
                                                {
                                                    mensajeNotificacion="Felicidades, has alcanzado tu meta de peso 1 día antes de lo planeado! ¿Deseas establecer una nueva meta?" ;
                                                }
                                                muestraConfirmacion(mensajeNotificacion,accionCambiarMetaCallback,"Redustat", "Cancelar,Aceptar");

                                            }
                                            else
                                            {
                                                muestraNotificacion("Se ha registrado el peso del día de hoy" , accionFinalCallback, "Redustat", "Aceptar");
                                            }
                                            
                                        }
                                    }
                                    
                            }
                            else
                            {
                                    muestraNotificacion("Se ha registrado el peso del día de hoy" , accionFinalCallback, "Redustat", "Aceptar");

                            }
                             function accionFinalCallback()
                             {
                               datosUsuarioController.menu();
                             }
                              function accionCambiarMetaCallback(botonPresionadoParam)
                             {
                                configuracionUsuario.save(desactivaMetaCallback);

                                function desactivaMetaCallback()
                                {

                                    if(botonPresionadoParam==2)
                                    {
                                        var controlador = new OperacionesPeso();
                                        controlador.registroMeta();
                                    }
                                    else
                                    {
                                        datosUsuarioController.menu();
                                    }
                                }
                                
                             }
                        }                     
                    }
                }

            }
            
        }
        else
        {            
            muestraNotificacion(validador.mensajeError, null, "Redustat", "Aceptar");
        }
    },
    /**
    * Obtiene los datos de la forma de RegistroMeta y actualiza/inserta los datos nuevos en configuracionusuario en la base de datos
    * @return {void} 
    */         
    actualizaMeta : function()
    {
            var obj = this;
            var validador = new Validacion([
                [$('#peso'), '^\\d+(\\.(\\d)+)?$', "Peso tiene que ser número"], 
                [$('#peso'), '^\\d{1,3}(\\.\\d{1,3})?$', "Peso debe tener tres digitos en enteros y decimales"]
                ]);
            if(validador.validar())
            {

                        var historialpeso2 = new HistorialPeso();
                        historialpeso2.obtenerReciente(obtenerPesoActualCallback);
                        function obtenerPesoActualCallback()
                        {
                            if($('#peso').val() >= historialpeso2.peso)
                            {
                                muestraNotificacion("La meta debe ser menor a tu peso: "+ parseFloat(historialpeso2.peso)+"." , null, "Redustat", "Aceptar");
                            }

                            else
                            {
                                            var configuracionUsuario = new ConfiguracionUsuario();
                                            configuracionUsuario.usuario_id = 1;
                                            configuracionUsuario.fecha_registro = obtenerFechaHoyMeta();
                                            configuracionUsuario.peso_ideal = $('#peso').val();
                                            configuracionUsuario.factor_reduccion = $('input[name="meta"]:checked').val();
                                            configuracionUsuario.meta_activa = true;
                                            
                                            //calculamos la fecha objetivo 

                                            var fecha_objetivo =new Date();
                                            var historialpeso = new HistorialPeso();
                                            historialpeso.obtenerReciente(obtenerRecienteHistorialCallback);
                                            function obtenerRecienteHistorialCallback()
                                            {
                                                    var peso_inicial = parseFloat(historialpeso.peso);
                                                    var peso_ideal = parseFloat(configuracionUsuario.peso_ideal);
                                                    var factor_reduccion = parseFloat(configuracionUsuario.factor_reduccion);
                                                    var dias = Math.round(((peso_inicial - peso_ideal) / factor_reduccion) * 7);

                                                    fecha_objetivo.setDate(fecha_objetivo.getDate()+dias);

                                                    var fechaParseada = dateToStringFormat(fecha_objetivo);  

                                                    configuracionUsuario.fecha_objetivo = fechaParseada;

                                                    configuracionUsuario.save(controllerCallback);
                                                    function controllerCallback()
                                                    {
                                                        var datosUsuarioController = new DatosUsuario();
                                                        muestraNotificacion("Se ha registrado tu meta" , null, "Redustat", "Aceptar");
                                                        datosUsuarioController.menu();
                                                    }

                                            }

                            }
                        }


                        
            }
            else
            {   
                muestraNotificacion(validador.mensajeError, null, "Redustat", "Aceptar");
            }

           

    }

}
    
