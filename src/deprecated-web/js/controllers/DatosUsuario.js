/**
 * Crea una instancia de DatosUsuario
 * @constructor
 * @this {DatosUsuario}
 */
function DatosUsuario()
{
    /** @private */this.diasFaltantesMeta = 0;
}

DatosUsuario.prototype =
{
    /**
    * Crea la base de datos llamando el método inittables de Database 
    * En caso de ser un usuario lo lleva al registro, si no, carga el menú
    * necesaria
    * @return {void} 
    */    
    index : function()
    {
        var obj = this;
        //Inicializamos la base de datos
        var database = new Database();
        database.initTables();
        
        //Checamos por un usuario existente
        var usuario = new Usuario();
        usuario.getRecords(controllerCallback);
        
        function controllerCallback(coleccion)
        {
            if(coleccion.length > 0)
            {
                obj.menu();
            }
            else
            {
                obj.formaRegistroUsuario();
            }
        }
    },
    /**
    * Carga la vista de registro de nuevo usuario
    * @return {void} 
    */    
    formaRegistroUsuario: function()
    {
        var view = new FormaRegistroUsuarioView();
        view.loadView();
    },
    /**
    * Toma los datos de la forma y registra al nuevo usuario en caso de que sean válidos
    * @return {void} 
    */    
    registraUsuario: function()
    {
        var obj = this;
        var usuario = new Usuario();
        var validador = new Validacion([
            [$('#peso'), '^\\d+(\\.(\\d)+)?$', "Peso tiene que ser número"], 
            [$('#peso'), '^\\d{1,3}(\\.\\d{1,3})?$', "Peso debe tener tres digitos en enteros y decimales"], 
            [$('#estatura'), '\\d+', "Estatura tiene que ser número"],
            [$('#estatura'), '^\\d{2,3}(\\.\\d{1})?$', "Estatura tiene que ser número con mínimo 2 y máximo 3 enteros y un decimal"],
            [$('#fecha_nac'), '^\\d{4}-((0\\d)|(1[012]))-(([012]\\d)|3[01])$', "Falta indicar fecha de nacimiento"]
            ]);
        if(validador.validar())
        {
            usuario.peso = $('#peso').val();
            usuario.estatura = $('#estatura').val();
            usuario.fechaNacimiento = $('#fecha_nac').val();
            usuario.genero = $('#genero').val();

            usuario.save(saveCallback);
            function saveCallback()
            {
                //Agregamos el registro en el historial de peso
                
                var historialpeso = new HistorialPeso();
                historialpeso.usuario_id = usuario.id;
                historialpeso.fecha_registro = obtenerFechaHoy();
                historialpeso.peso = $('#peso').val();
                historialpeso.save(saveHistorialPesoCallback);
                function saveHistorialPesoCallback()
                {    
                    muestraNotificacion("Bienvenido a Redustat" , null, "Redustat", "Aceptar");
                    obj.menu();
                }
            }
        }
        else
        {            
            muestraNotificacion(validador.mensajeError, null, "Redustat", "Aceptar");
        }
    },
    /**
    * Prepara la información necesaria del menú
    * @return {void} 
    */    
    menu: function()
    {

        var obj = this;
        var historialpeso = new HistorialPeso();        
        historialpeso.obtenerReciente(controllerCallback);
        function controllerCallback()
        {
            obj.obtieneDiasFaltantesParaMeta(resultadoCallback);
            function resultadoCallback(resultado)
            {
                var historialpesoinicial = new HistorialPeso();
                historialpesoinicial.obtenerPrimero(historialpesoinicialObtenerPrimeroCallback);
                function historialpesoinicialObtenerPrimeroCallback()
                {
                    var kilos = historialpesoinicial.peso - historialpeso.peso;
                    kilos =  Math.round( parseFloat(kilos) * 10 ) / 10;
                    var mensaje = "";
                    if(kilos >= 0)
                    {
                        mensaje = "Desde el inicio has bajado: <font color=#186400 weight=1000>"+kilos+" kg!<\/font>";
                    }
                    else
                    {
                        kilos = -kilos;
                        mensaje = "Desde el inicio has subido: <font color=red weight=1000>"+kilos+" kg!<\/font>";
                    }
                    
                    var data = {
                        peso: (Math.round( parseFloat(historialpeso.peso) * 10 ) / 10),
                        peso_meta : resultado.meta,
                        dias_faltantes : resultado.dias,
                        mensaje : mensaje
                    };

                    if(data.dias_faltantes==1)
                    {
                        data.texto_dias = " d&iacute;a";
                    }
                    else
                    {
                        data.texto_dias = " d&iacute;as";

                    }

                    var view = new Menu(data);
                    view.loadView();
                }
            }
        }
    },
    /**
    * Solicita al modelo encontrar al usuario y carga y llama la vista para editar usuario 
    * @return {void} 
    */
    editarUsuario: function()
    {
        var usuario = new Usuario();
        usuario.find(1, controllerCallback);
        function controllerCallback()
        {
            
            var historialpeso = new HistorialPeso();
            historialpeso.obtenerPrimero(obtenerPrimeroCallback);
            function obtenerPrimeroCallback()
            {
                usuario.peso = historialpeso.peso;
                var data = {
                    usuario: usuario
                };
                var view = new FormaActualizaUsuario(data);
                view.loadView();
            }
        }
    },
    /**
    * Obtiene los datos de la forma de edición de usuario y en caso de ser válidos actualiza el registro
    * en la tabla Usuario
    * @return {void} 
    */    
    actualizarUsuario : function()
    {
        var obj = this;
        var validador = new Validacion([
            [$('#peso'), '^\\d+(\\.(\\d)+)?$', "Peso tiene que ser número"], 
            [$('#peso'), '^\\d{1,3}(\\.\\d{1,3})?$', "Peso debe tener tres digitos en enteros y decimales"], 
            [$('#estatura'), '\\d+', "Estatura tiene que ser número"],
            [$('#estatura'), '^\\d{1,3}(\\.\\d{1})?$', "Estatura tiene que ser número con máximo 3 enteros y un decimal"],
            [$('#fecha_nac'), '^\\d{4}-((0\\d)|(1[012]))-(([012]\\d)|3[01])$', "Falta indicar fecha de nacimiento"]
            ]);
        
        if(validador.validar())
        {
            var usuario = new Usuario();
            usuario.find(1, controllerCallback);
            function controllerCallback()
            {
                usuario.peso = $('#peso').val();
                usuario.estatura = $('#estatura').val();
                usuario.fechaNacimiento = $('#fecha_nac').val();
                usuario.genero = $('#genero').val();
                usuario.save(saveCallback);
                function saveCallback()
                {
                    //Agregamos el registro en el historial de peso
                
                    var historialpeso = new HistorialPeso();
                    historialpeso.obtenerPrimero(obtenerPrimeroCallback);
                    function obtenerPrimeroCallback()
                    {
                        historialpeso.peso = $('#peso').val();
                        

                        historialpeso.validaFecha(validaFechaCallback);
            function validaFechaCallback(validoHoy)
            {
                if(!validoHoy)
                {
                    muestraConfirmacion("Ya tienes un peso registrado hoy. ¿Quieres sustituirlo con este nuevo?", sobreescribirCallback,  "Redustat", ["Cancelar","Aceptar"]);
                    function sobreescribirCallback(button)
                    {
                        if(button == 2)
                        {
                            historialpeso.peso = $('#peso').val();
                            guardaPeso();
                        }
                        
                    }
                    
                }
                else
                {
                    guardaPeso();
                }
                function guardaPeso()
                {
                    historialpeso.save(saveHistorialPesoCallback);
                    var fechaDeHoy= new Date();
                    function saveHistorialPesoCallback()
                    {

                        var datosUsuarioController = new DatosUsuario();

                        var pesoActual = historialpeso.peso;
                        var configuracionUsuario = new ConfiguracionUsuario();
                        configuracionUsuario.obtenerReciente(obtenerRecienteConfiguracionCallback);

                        function obtenerRecienteConfiguracionCallback()
                        {

                            if(configuracionUsuario.meta_activa == "true")
                            {
                                    // ejecutamos save para guardar false en meta_activa del usuario
                                    if(Number(pesoActual) <= Number(configuracionUsuario.peso_ideal))
                                        {                                            
                                            configuracionUsuario.meta_activa=false;
                                        }

                                    configuracionUsuario.save(resetPesoidealConfiguracionUsuarioCallback)

                                    function resetPesoidealConfiguracionUsuarioCallback()
                                    {
                                            var mensajeNotificacion="";
                                            
                                            
                                        if(fechaDeHoy >= parseDate(dateToString(configuracionUsuario.fecha_objetivo)))
                                        {
                                            configuracionUsuario.meta_activa=false;


                                            if(Number(pesoActual) <= Number(configuracionUsuario.peso_ideal) && daydiff(fechaDeHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo))) <1)  
                                            {
                                                mensajeNotificacion="Felicidades, has alcanzado tu meta de peso a tiempo ¿Deseas establecer una nueva meta?";
                                            }
                                            else if(Number(pesoActual) <= Number(configuracionUsuario.peso_ideal))
                                            {
                                                if(Math.floor(daydiff(fechaDeHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo)))) > 1)
                                                {
                                                    mensajeNotificacion="Has alcanzado tu meta de peso "+Math.floor(daydiff(fechaDeHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo)))) +" días después de lo planeado. ¿Deseas establecer una nueva meta?" ;
                                                }
                                                else
                                                {
                                                    mensajeNotificacion="Has alcanzado tu meta de peso 1 día después de lo planeado. ¿Deseas establecer una nueva meta?" ;
                                                }
                                            }
                                            else 
                                            {
                                                mensajeNotificacion="No has alcanzado tu meta de peso ¿Deseas establecer una nueva meta?" ;
                                            }
                                            
                                            muestraConfirmacion(mensajeNotificacion,accionCambiarMetaCallback,"Redustat", "Cancelar,Aceptar");

                                        }
                                        else
                                        {

                                            if(Number(pesoActual) <= Number(configuracionUsuario.peso_ideal)) 
                                            {
                                                if(Math.floor(daydiff(fechaDeHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo))))>1)
                                                {
                                                    mensajeNotificacion="Felicidades, has alcanzado tu meta de peso "+Math.floor(daydiff(fechaDeHoy,parseDate(dateToString(configuracionUsuario.fecha_objetivo)))) +" días antes de lo planeado! ¿Deseas establecer una nueva meta?" ;

                                                }
                                                else
                                                {
                                                    mensajeNotificacion="Felicidades, has alcanzado tu meta de peso 1 día antes de lo planeado! ¿Deseas establecer una nueva meta?" ;
                                                }
                                                muestraConfirmacion(mensajeNotificacion,accionCambiarMetaCallback,"Redustat", "Cancelar,Aceptar");

                                            }
                                            else
                                            {
                                                muestraNotificacion("Se ha registrado el peso del día de hoy" , accionFinalCallback, "Redustat", "Aceptar");
                                            }
                                            
                                        }
                                    }
                                    
                            }
                            else
                            {
                                    muestraNotificacion("Se ha registrado el peso del día de hoy" , accionFinalCallback, "Redustat", "Aceptar");

                            }
                             function accionFinalCallback()
                             {
                               datosUsuarioController.menu();
                             }
                              function accionCambiarMetaCallback(botonPresionadoParam)
                             {
                                configuracionUsuario.save(desactivaMetaCallback);

                                function desactivaMetaCallback()
                                {

                                    if(botonPresionadoParam==2)
                                    {
                                        var controlador = new OperacionesPeso();
                                        controlador.registroMeta();
                                    }
                                    else
                                    {
                                        datosUsuarioController.menu();
                                    }
                                }
                                
                             }
                        }                     
                    }
                }
                        }
                    }
                }
                
            }
        }
        else
        {
            muestraNotificacion(validador.mensajeError, null, "Redustat", "Aceptar");
        }
    },
    /**
    * Calcula los días faltantes para llegar a la meta
    * @param {function} callback función a ejecutar al terminar el cálculo de los días
    * @return {void} 
    */
    obtieneDiasFaltantesParaMeta : function(callback)
    {
        var fechaDeHoy = new Date();
            var resultado = {};
            var configuracionusuario = new ConfiguracionUsuario();
            configuracionusuario.obtenerReciente(obtenerRecienteConfiguracionCallback);
            function obtenerRecienteConfiguracionCallback()
            {
                if(configuracionusuario.peso_ideal == null || configuracionusuario.meta_activa == "false")
                {
                    resultado['dias'] = 0;
                    resultado['meta'] = 'N/A';
                }
                else
                {
                   resultado['meta'] = Math.round( parseFloat(configuracionusuario.peso_ideal) * 10 ) / 10;

                   var d = new Date(fechaDeHoy);

                   d.setDate(d.getDate()-1);


                   var fechaObjetivo = parseDate(dateToString(configuracionusuario.fecha_objetivo));
                   
                   resultado['dias'] = Math.floor(daydiff(d, fechaObjetivo));

                   // si hay menos de un día se muestra la propuesta de registrar el peso

                   if(fechaDeHoy >= fechaObjetivo)
                   {

                        if(configuracionusuario.fecha_notificacion && configuracionusuario.fecha_notificacion != "undefined")
                        {   
                            if(daydiff(fechaDeHoy, parseDate(dateToString(configuracionusuario.fecha_notificacion)))>1)
                            {
                                muestraConfirmacion("Se ha cumplido tu tiempo meta, ¿Deseas registrar tu peso?",accionCambiarMetaCallback,"Redustat", "Cancelar,Aceptar");
                            }
                                resultado['dias'] = 0;

                        } 
                        else
                        {
                                muestraConfirmacion("Se ha cumplido tu tiempo meta, ¿Deseas registrar tu peso?",accionCambiarMetaCallback,"Redustat", "Cancelar,Aceptar");
                                resultado['dias'] = 0;
                        }


                        function accionCambiarMetaCallback (botonPresionadoParam)
                       {


                        configuracionusuario.fecha_notificacion = dateToStringFormat(fechaDeHoy);

                            configuracionusuario.save(null);

                            if (botonPresionadoParam == 2)
                            {
                                var controlador = new OperacionesPeso();
                                controlador.registroPeso();
                            }
                       } 

                   } 
                  

                   

                }

                callback(resultado);
            }
        
    }
}