/**
 * Crea una instancia de Contenidos
 * @constructor
 * @this {Contenidos}
 */
function Contenidos()
{
    
}

Contenidos.prototype =
{
    /**
     * Solicita las categorias al modelo CategoriaAlimento y al obtenerlas 
     * crea una instancia de la vista ListaCategorias enviándole la información 
     * necesaria
     * @return {void} 
     */
    listaCategorias: function()
    {
        var categoriaAlimento =  new CategoriaAlimento();
        categoriaAlimento.getRecords(controllerCallback);
        function controllerCallback(coleccion)
        {
            var view  = new ListaCategorias(coleccion);
            view.loadView();
        }
    },
    /**
     * Solicita los alimentos pertenecientes a la categoria id al modelo Alimento 
     * y al obtenerlas crea una instancia de la vista ListaAlimentos enviándole la 
     * información necesaria
     * @param {String} idParam identificador de la categoria 
     * @param {String} nombreCategoriaParam nombre de la categoria
     * @return {void} 
     */    
    listaAlimentos: function(idParam, nombreCategoriaParam)
    {
        var alimento =  new Alimento();
        alimento.find(idParam, controllerCallback);
        function controllerCallback(coleccion)
        {
            var data = {
                coleccion : coleccion,
                nombreCategoria: nombreCategoriaParam
            }
            var view  = new ListaAlimentos(data);
            view.loadView();
        }
    },
    /**
     * Solicita los tips al modelo Tip 
     * y al obtenerlas crea una instancia de la vista ListaTips enviándole la 
     * información necesaria
     * @return {void} 
     */      
     mostrarTips: function()
    {
        var tip =  new Tip();
        tip.getRecords(controllerCallback);
        function controllerCallback(coleccion)
        {

            var data = {
                    tip: tip
                };
            
            var view  = new ListaTips(coleccion);
            view.loadView();
        }
    },
    /**
     * Solicita información sobre un tip al obtener la información 
     * al obtenerla se crea una instanca de TipSencillo, se le envía
     * información necesaria y se carga su vista.
     * @param {String} idParam cadena de caracteres a encriptar
     * @return {void} 
     */      
    mostrarTip: function(idParam)
    {
        var tip =  new Tip();
        tip.find(idParam, controllerCallback);
        function controllerCallback()
        {

            var data = {
                    tip: tip
                };
            
            var view  = new TipSencillo(data);
            view.loadView();
        }
    }
    
}