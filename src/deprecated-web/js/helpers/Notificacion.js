
function muestraNotificacion(contenidoNotificacionParam, callbackNotificacionParam, tituloNotificacionParam, arregloBotonesNotificacionParam)
{
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) 
    {
        navigator.notification.alert( contenidoNotificacionParam, callbackNotificacionParam, tituloNotificacionParam, arregloBotonesNotificacionParam);
    }
    else
    {
        if(callbackNotificacionParam == null)
        {
            alert(contenidoNotificacionParam);
            if(callbackNotificacionParam != null)
            {
                callbackNotificacionParam();
            }
        }
        else
        {
            var respuesta = confirm(contenidoNotificacionParam);
            if(callbackNotificacionParam != null && respuesta == true)
            {
                callbackNotificacionParam();
            }
        }
    }
}


function muestraConfirmacion(contenidoNotificacionParam, callbackNotificacionParam, tituloNotificacionParam, arregloBotonesNotificacionParam)
{
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) 
    {
        navigator.notification.confirm( contenidoNotificacionParam, callbackNotificacionParam, tituloNotificacionParam, arregloBotonesNotificacionParam);
    }
    else
    {
        if(callbackNotificacionParam == null)
        {
            alert(contenidoNotificacionParam);
            if(callbackNotificacionParam != null)
            {
                callbackNotificacionParam();
            }
        }
        else
        {
            var respuesta = confirm(contenidoNotificacionParam);
            if(callbackNotificacionParam != null && respuesta == true)
            {
                callbackNotificacionParam();
            }
        }
    }
}