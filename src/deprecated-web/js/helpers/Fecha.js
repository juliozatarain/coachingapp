var hoy = new Date();

function obtenerFechaHoy()
{

    var date = new Date();
    var year = date.getFullYear().toString();
    var month = (date.getMonth()+1).toString();
    var day = date.getDate().toString();
    
    if (parseInt(month) < 10) month = "0" + month;
    if (parseInt(day) < 10) day = "0" + day;

    var parsedDate = year + month + day;
    return parsedDate;
                
}
function obtenerFechaHoyMeta()
{

    var date = new Date(hoy.getTime());
    var year = date.getFullYear().toString();
    var month = (date.getMonth()+1).toString();
    var day = date.getDate().toString();
    
    if (parseInt(month) < 10) month = "0" + month;
    if (parseInt(day) < 10) day = "0" + day;

    var parsedDate = year + month + day;

    return parsedDate;
                
}
function suma1dia()
{
        hoy.setDate(hoy.getDate() + 1);
        alert(hoy);

}


function parseDate(str) 
{
    return new Date(str.substr(0,4), String(Number(str.substr(4,2))-1), str.substr(6,2));
}
function dateToStringFormat(fecha)
{
    var date = new Date(fecha.getTime());
    var year = date.getFullYear().toString();
    var month = (date.getMonth()+1).toString();
    var day = date.getDate().toString();
    
    if (parseInt(month) < 10) month = "0" + month;
    if (parseInt(day) < 10) day = "0" + day;

    var parsedDate = year + month + day;
    return parsedDate;
}
function dateToString(str)
{
    
    return ""+str.toString();
}

function daydiff(first, second) 
{


    return Math.abs(second-first)/(1000*60*60*24);

}