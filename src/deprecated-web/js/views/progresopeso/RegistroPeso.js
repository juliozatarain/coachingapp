/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {RegistroPeso}
 */
function RegistroPeso()
{
    this.view="";
    this.view += "<div class=\"logo_big\">";
    this.view += "    	<img src=\"images\/logo.png\" \/>";
    this.view += "<\/div>";
    this.view += "	<div class=\"content\">";
    this.view += "    	<div class=\"texto\">";
    this.view += "        	¿Cu&aacute;nto pesaste hoy?<br \/><br \/>";
    this.view += "			¡Registra tu peso para compararlo con d&iacute;as anteriores!<br \/><br \/><br \/><br \/>";
    this.view += "        <\/div>";
    this.view += "        <div class=\"forma\">";
    this.view += "        	<table cellpadding=\"2\">";
    this.view += "            	<tr>";
    this.view += "                	<td style=\"text-align:center\">Peso de hoy:<\/td>";
    this.view += "                <\/tr>";
    this.view += "                <tr>";
    this.view += "                	<td><input type=\"number\" id=\"peso\" name=\"peso\" placeholder=\"Tu peso en kgs\" style=\"text-align:center\"\/><\/td>";
    this.view += "                <\/tr>";
    this.view += "               ";
    this.view += "                ";
    this.view += "			<\/table>";
    this.view += "        <\/div>";
    this.view += "    <\/div>";
    this.view += "    <div class=\"botones\">";
    this.view += "   	  <a href=#><div class=\"btn_izq\"><strong>Regresar<\/strong><\/div><\/a>";
    this.view += "        <a href=#><div class=\"btn_der\"><strong>Guardar<\/strong><\/div><\/a>";
    this.view += "    <\/div>";    
}

RegistroPeso.prototype =
{
    /**
    * Bindings de botones.
    * @return {void} 
    */      
    loadView: function()
    {
        $("body").css("opacity","0");
        $('#home').html(this.view);
               
        animaVista(transitionReady);

        function transitionReady()
        {


            $("#peso")[0].oninput = function () {
                if (this.value.length > 7)
                    this.value = this.value.slice(0,7); 
            };
            
            $('div.btn_izq').tap(function(){
                var controlador = new DatosUsuario();
                controlador.menu();
            });
            
            $('div.btn_der').tap(function(){
                var controlador = new OperacionesPeso();
                controlador.actualizaPeso();
            });
            
            $('div.btn_der').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_der').on('touchend', function(){
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_der').on('touchmove', function(){
                $(this).removeClass('btn_pressed') 
            });
                
            $('div.btn_izq').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_izq').on('touchend', function(){
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_izq').on('touchmove', function(){
                $(this).removeClass('btn_pressed') 
            });
        }
    }
}