/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {RegistroMeta}
 */
function RegistroMeta()
{
    this.view="";
    this.view += "	<div class=\"logo_big\">";
    this.view += "    	<img src=\"images\/logo.png\" \/>";
    this.view += "<\/div>";
    this.view += "	<div class=\"content\">";
    this.view += "    	<div class=\"texto\">";
    this.view += "        	¡Logra tus metas con Redustat!";
    this.view += "        <\/div>";
    this.view += "        <div class=\"forma\">";
    this.view += "        	<table cellpadding=\"2\">";
    this.view += "            	<tr>";
    this.view += "                	<td width=\"18%\">Peso:<\/td>";
    this.view += "                    <td width=\"82%\"><input type=\"number\" id=\"peso\" name=\"peso\" placeholder=\"Mi peso ideal en kgs\"\/><\/td>";
    this.view += "                <\/tr>";
    this.view += "			<\/table>";
    this.view += "        <\/div>";
    this.view += "        <div class=\"texto\">";
    this.view += "        	Quiero llegar a mi peso ideal bajando:";
    this.view += "        <\/div>";
    this.view += "        <div class=\"forma\" style=\"text-align:left\">";
    this.view += "        	<ul class=\"listaRadio\">";
    this.view += "            	<li>";
    this.view += "                    <div class=\"radioCustom\">";
    this.view += "                        <input type=\"radio\" name=\"meta\" id=\"radioCustom\" value=\"1.00\"\/>";
    this.view += "                        <label for=\"radioCustom\"><\/label>";
    this.view += "                        <div class=\"textoRadioCustom\"> 1.00kg x semana <\/div>";
    this.view += "                    <\/div>";
    this.view += "                <\/li>";
    this.view += "                <li>";
    this.view += "                    <div class=\"radioCustom\">";
    this.view += "                        <input type=\"radio\" name=\"meta\" id=\"radioCustom2\" value=\"0.75\"\/>";
    this.view += "                        <label for=\"radioCustom2\"><\/label>";
    this.view += "                        <div class=\"textoRadioCustom\"> 0.75kg x semana <\/div>";
    this.view += "                    <\/div>";
    this.view += "                <\/li>";
    this.view += "                <li>";
    this.view += "                    <div class=\"radioCustom\">";
    this.view += "                        <input type=\"radio\" name=\"meta\" id=\"radioCustom3\" value=\"0.5\"\/>";
    this.view += "                        <label for=\"radioCustom3\"><\/label>";
    this.view += "                        <div class=\"textoRadioCustom\"> 0.50kg x semana <\/div>";
    this.view += "                    <\/div>";
    this.view += "                <\/li>";
    this.view += "                 <li>";
    this.view += "                    <div class=\"radioCustom\">";
    this.view += "                        <input type=\"radio\" name=\"meta\" id=\"radioCustom4\" value=\"0.25\"\/>";
    this.view += "                        <label for=\"radioCustom4\"><\/label>";
    this.view += "                        <div class=\"textoRadioCustom\"> 0.25kg x semana <\/div>";
    this.view += "                    <\/div>";
    this.view += "                <\/li>";
    this.view += "			<\/ul>";
    this.view += "        <\/div>";
    this.view += "    <\/div>";
    this.view += "    <div class=\"botones\">";
    this.view += "   		<a href=#><div class=\"btn_izq\"><strong>Regresar<\/strong><\/div><\/a>";
    this.view += "        <a href=#><div class=\"btn_der\"><strong>Guardar<\/strong><\/div><\/a>";
    this.view += "    <\/div>";

}

RegistroMeta.prototype =
{
    /**
    * Bindings de botones.
    * @return {void} 
    */      
    loadView: function()
    {
        $("body").css("opacity","0");
        $('#home').html(this.view);
               

        animaVista(transitionReady);

        function transitionReady()
        {

            $("#peso")[0].oninput = function () {
                if (this.value.length > 7)
                    this.value = this.value.slice(0,7); 
            };
            
            $('div.btn_izq').tap(function(){
                var controlador = new DatosUsuario();
                controlador.menu();
            });
            
            $('div.btn_der').tap(function(){
                var controlador = new OperacionesPeso();
                controlador.actualizaMeta();
            });
            
            $('div.btn_der').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_der').on('touchend', function(){
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_der').on('touchmove', function(){
                $(this).removeClass('btn_pressed') 
            });
                
            $('div.btn_izq').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_izq').on('touchend', function(){
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_izq').on('touchmove', function(){
                $(this).removeClass('btn_pressed') 
            });

             ///******* touchstate lista
            $('.listaRadio li').on("touchstart", function(){

                $(this).css("background-color","lightgrey");
            });
             $('.listaRadio li').on("touchmove", function(){

                $(this).css("background-color","transparent");
            });
              $('.listaRadio li').on("touchend", function(){

                $(this).css("background-color","transparent");
            });
            ///*******
        }
    }

}