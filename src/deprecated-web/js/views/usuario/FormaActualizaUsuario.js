/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {FormaActualizaUsuarioView}
 */
 function FormaActualizaUsuario(data)
{
    this.view="";
    this.view += "<div class=\"logo_big\">";
    this.view += "    	<img src=\"images\/logo.png\" \/>";
    this.view += "    <\/div>";
    this.view += "	<div class=\"content\">";
    this.view += "    	<div class=\"texto\">";
    this.view += "        	Actualiza tus datos<br \/><br \/>";
    this.view += "			¡Recuerda que debes poner el peso con el que iniciaste el programa!";
    this.view += "        <\/div>";
    this.view += "        <div class=\"forma\">";
    this.view += "        	<table cellpadding=\"2\">";
    this.view += "            	<tr>";
    this.view += "                	<td width=\"18%\">Fecha:<\/td>";
    this.view += "                    <td width=\"82%\"><input type=\"text\" id=\"fecha_nac\" name=\"fecha_nac\" placeholder=\"Fecha de nacimiento\" value=\""+data.usuario.fechaNacimiento+"\"><\/td>";
    this.view += "                <\/tr>";
    this.view += "                <tr>";
    this.view += "                	<td>Peso:<\/td>";
    this.view += "                    <td><input type=\"number\" id=\"peso\" name=\"peso\" placeholder=\"Tu peso en kgs\" value=\""+data.usuario.peso+"\"/><\/td>";
    this.view += "                <\/tr>";
    this.view += "                <tr>";
    this.view += "                	<td>Estatura:<\/td>";
    this.view += "                    <td><input type=\"number\" id=\"estatura\" name=\"estatura\" placeholder=\"Tu altura en cms\" value=\""+data.usuario.estatura+"\" /><\/td>";
    this.view += "                <\/tr>";
    this.view += "            	<tr>";
    this.view += "                	<td>G&eacute;nero:<\/td>";
    this.view += "                <\/tr>";
    this.view += "                <tr>";
    this.view += "                	<td colspan=\"2\" style=\"text-align:left\">";
    this.view += "                		<select name=\"genero\" id=\"genero\">";
    if(data.usuario.genero == 0)
    {
        this.view += "                              <option value=\"1\">Femenino<\/option>";
        this.view += "                    		<option value=\"0\" selected=\"selected\">Masculino<\/option>";
    }
    else
    {
        this.view += "                              <option value=\"1\" selected=\"selected\">Femenino<\/option>";
        this.view += "                    		<option value=\"0\">Masculino<\/option>";
    }
    this.view += "                		<\/select>";
    this.view += "                    <\/td>";
    this.view += "                <\/tr>";
    this.view += "			<\/table>";
    this.view += "        <\/div>";
    this.view += "    <\/div>";
    this.view += "    <div class=\"botones\">";
    this.view += "   	  <a href=#><div class=\"btn_izq\"><strong>Regresar<\/strong><\/div><\/a>";
    this.view += "        <a href=#><div class=\"btn_der\"><strong>Guardar<\/strong><\/div><\/a>";
    this.view += "    <\/div>";

}

FormaActualizaUsuario.prototype =
{
    /**
    * Bindings de botones, función de búsqueda.
    * @return {void} 
    */         
    loadView: function()
    {
        $("body").css("opacity","0");


        $('#home').html(this.view);

        animaVista(transitionReady);
        //función que se ejecuta al terminar la transición
        function transitionReady()
        {
        
            $("#peso")[0].oninput = function () {
                if (this.value.length > 7)
                    this.value = this.value.slice(0,7); 
            };
            
            $("#estatura")[0].oninput = function () {
                if (this.value.length > 5)
                    this.value = this.value.slice(0,5); 
            };

            
            //Binding del control de fecha
            $('input#fecha_nac').mobiscroll().date({
                theme: 'default',
                display: 'bottom',
                mode: 'scroller',
                dateOrder: 'mmD ddyy',
                dateFormat: 'yyyy-mm-dd',
                lang : 'es',
                maxDate : new Date()
            });    
            
            $('div.btn_izq').tap(function(){
                var controlador = new DatosUsuario();
                controlador.menu();
            });
            
           
            $('div.btn_der').tap(function(){
                var controlador = new DatosUsuario();
                controlador.actualizarUsuario();
            });
            
            
            $('div.btn_der').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_der').on('touchend', function(){
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_der').on('touchmove', function(){
                $(this).removeClass('btn_pressed') 
            });
                
            $('div.btn_izq').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_izq').on('touchend', function(){
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_izq').on('touchmove', function(){
                $(this).removeClass('btn_pressed') 
            });
            
        }
    }
    
}