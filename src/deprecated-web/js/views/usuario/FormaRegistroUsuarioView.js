/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {FormaRegistroUsuarioView}
 */
function FormaRegistroUsuarioView(data)
{
    this.view="";
    this.view += "<div class=\"logo_big\">";
    this.view += "    	<img src=\"images\/logo.png\" \/>";
    this.view += "    <\/div>";
    this.view += "	<div class=\"content\">";
    this.view += "    	<div class=\"texto\">";
    this.view += "        	Bienvenido!<br \/><br \/>";
    this.view += "			Antes de usar la aplicación debes";
    this.view += "			proporcionarnos unos datos básicos.";
    this.view += "        <\/div>";
    this.view += "        <div class=\"forma\">";
    this.view += "        	<table cellpadding=\"2\">";
    this.view += "            	<tr>";
    this.view += "                	<td width=\"18%\">Fecha:<\/td>";
    this.view += "                    <td width=\"82%\"><input type=\"text\" name=\"fecha_nac\" id=\"fecha_nac\" placeholder=\"Fecha de nacimiento\"><\/td>";
    this.view += "                <\/tr>";
    this.view += "                <tr>";
    this.view += "                	<td>Peso:<\/td>";
    this.view += "                    <td><input type=\"number\" maxlength=\"7\" id=\"peso\" name=\"peso\" placeholder=\"Tu peso en kgs\"\/><\/td>";
    this.view += "                <\/tr>";
    this.view += "                <tr>";
    this.view += "                	<td>Estatura:<\/td>";
    this.view += "                    <td><input type=\"number\" id=\"estatura\" name=\"estatura\" placeholder=\"Tu altura en cms\"\/><\/td>";
    this.view += "                <\/tr>";
    this.view += "            	<tr>";
    this.view += "                	<td>G&eacute;nero:<\/td>";
    this.view += "                <\/tr>";
    this.view += "                <tr>";
    this.view += "                	<td colspan=\"2\" style=\"text-align:left\">";
    this.view += "                		<select id=\"genero\" name=\"genero\">";
    this.view += "                			<option value=\"1\">Femenino<\/option>";
    this.view += "                    		<option value=\"0\">Masculino<\/option>";
    this.view += "                		<\/select>";
    this.view += "                    <\/td>";
    this.view += "                <\/tr>";
    this.view += "			<\/table>";
    this.view += "        <\/div>";
    this.view += "    <\/div>";
    this.view += "    <div class=\"botones\">";
    this.view += "    	";
    this.view += "        <a href=#><div class=\"btn_der\"><strong>Comenzar<\/strong><\/div><\/a>";
    this.view += "    <\/div>";
    this.view += "";


}

FormaRegistroUsuarioView.prototype = 
{
    /**
    * Bindings de botones.
    * @return {void} 
    */      
    loadView: function()
    {
        var obj = this;
        
        $("body").css("opacity","0");
        $('#home').html(obj.view);

        animaVista(transitionReady);

        function transitionReady()
        {            
            $("#peso")[0].oninput = function () {
                if (this.value.length > 7)
                    this.value = this.value.slice(0,7); 
            };
            
            $("#estatura")[0].oninput = function () {
                if (this.value.length > 5)
                    this.value = this.value.slice(0,5); 
            };


            $('div.btn_der').tap(function(){
                var controlador = new DatosUsuario();       
                controlador.registraUsuario();
            });
            
            //Binding del control de fecha
            $('input#fecha_nac').mobiscroll().date({
                theme: 'default',
                display: 'bottom',
                mode: 'scroller',
                dateOrder: 'mmD ddyy',
                dateFormat: 'yyyy-mm-dd',
                lang : 'es',
                maxDate : new Date()
            
            });    
            
            $('div.btn_der').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
            
            $('div.btn_der').on('touchend', function(){
                $(this).removeClass('btn_pressed'); 
            })
            
            $('div.btn_der').on('touchmove', function(){
                $(this).removeClass('btn_pressed'); 
            });

        }
    }
}
