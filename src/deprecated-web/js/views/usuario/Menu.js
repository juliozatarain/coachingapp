/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {Menu}
 */
function Menu(data)
{
    this.view="";
    this.view += "<div class=\"logo_small\">";
    this.view += "    	<img src=\"images\/logo.png\" \/>";
    this.view += "	<\/div>";
    this.view += "   	<a href=\"#\">";
    this.view += "    	<div id=\"btn_config\" class=\"btn_config\">";
    this.view += "   			<img id=\"btn_config_img\" src=\"images\/config_btn.png\" \/>";
    this.view += "  		<\/div>";
    this.view += "    <\/a>";
    this.view += "	<div class=\"content\">";
    this.view += "    	<div class=\"full_banner\">";
    this.view +=        data.mensaje;
    this.view += "        <\/div>";
    this.view += "        <div class=\"recuadros_meta\">";
    this.view += "        	<div class=\"recuadro_izq\">";
    this.view += "            Mi peso<br \/>";
    this.view += "            <font size=\"5.7\">"+data.peso+"<\/font><br \/>";
    this.view += "            kg<br \/>";
    this.view += "            <\/div>";
    this.view += "            <div class=\"flecha_enmedio\">";
    this.view += "            <\/div>";
    this.view += "            <div class=\"recuadro_der\">";
    this.view += "            Mi meta<br \/>";
    this.view += "            <font size=\"5.7\" color=#186400>"+data.peso_meta+"<\/font>kg<br \/>";
    this.view += "            En <font color=#D20000>"+data.dias_faltantes+"<\/font>"+data.texto_dias+"<br \/>";
    this.view += "            <\/div>";
    this.view += "        <\/div>";
    this.view += "        <div class=\"recuadros_grafica\">";
    this.view += "        	<a href=\"#\">";
    this.view += "            	<div id=\"grafica_peso\" class=\"grafica_peso_img\">";
    this.view += "					<img id=\"grafica_peso_img\" src=\"images\/peso_btn.png\" \/>";
    this.view += "            	<\/div>";
    this.view += "            <\/a>";
    this.view += "            <a href=\"#\">";
    this.view += "            	<div id=\"grafica_imc\" class=\"grafica_imc_img\">";
    this.view += "					<img id=\"grafica_imc_img\" src=\"images\/imc_btn.png\" \/>";
    this.view += "            	<\/div>";
    this.view += "            <\/a>";
    this.view += "        <\/div>";
    this.view += "   	  	<div class=\"forma\" style=\"text-align:left\">";
    this.view += "        	<ul class=\"lista_links\">";
    this.view += "                	<li id=\"meta_peso\"><a href=\"#\">";
    this.view += "                    	<div class=\"elementoCategLink\">";
    this.view += "                        	<div class=\"nombreCateg\">";
    this.view += "                        		Mi meta de peso";
    this.view += "                        <\/div>";
    this.view += "                            <div class=\"flechaCateg\">";
    this.view += "                            <img src=\"images\/ico1-ipad.png\" \/>";
    this.view += "                            	<img src=\"images\/flecha.png\" \/>";
    this.view += "                            <\/div>";
    this.view += "                    	<\/div><\/a>";
    this.view += "                	<\/li>";
    this.view += "                ";
    this.view += "                ";
    this.view += "                	<li id=\"registrar_peso\"><a href=\"#\">";
    this.view += "                    	<div class=\"elementoCategLink\">";
    this.view += "                        	<div class=\"nombreCateg\">";
    this.view += "                        		Registrar mi peso";
    this.view += "                        <\/div>";
    this.view += "                            <div class=\"flechaCateg\">";
    this.view += "                            <img src=\"images\/ico2-ipad.png\" \/>";
    this.view += "                            	<img src=\"images\/flecha.png\" \/>";
    this.view += "                            <\/div>";
    this.view += "                    	<\/div><\/a>";
    this.view += "                	<\/li>";
    this.view += "                ";
    this.view += "                ";
    this.view += "                	<li id=\"contador_calorias\"><a href=\"#\">";
    this.view += "                    	<div class=\"elementoCategLink\">";
    this.view += "                        	<div class=\"nombreCateg\">";
    this.view += "                        		Contador de calorías";
    this.view += "                        <\/div>";
    this.view += "                            <div class=\"flechaCateg\">";
    this.view += "                            <img src=\"images\/ico3-ipad.png\" \/>";
    this.view += "                            	<img src=\"images\/flecha.png\" \/>";
    this.view += "                            <\/div>";
    this.view += "                    	<\/div><\/a>";
    this.view += "                	<\/li>";
    this.view += "                ";
    this.view += "                ";
    this.view += "                	<li id=\"tips\"><a href=\"#\">";
    this.view += "                    	<div class=\"elementoCategLink\">";
    this.view += "                        	<div class=\"nombreCateg\">";
    this.view += "                        		Recomendaciones y tips";
    this.view += "                        <\/div>";
    this.view += "                            <div class=\"flechaCateg\">";
    this.view += "                            	<img src=\"images\/ico8-ipad.png\" \/>";
    this.view += "                            	<img src=\"images\/flecha.png\" \/>";
    this.view += "                            <\/div>";
    this.view += "                    	<\/div><\/a>";
    this.view += "                	<\/li>     ";
    this.view += "		<\/ul>";
    this.view += "        <\/div>";
    this.view += "    <\/div>";
}

Menu.prototype = 
{
    /**
    * Bindings de botones.
    * @return {void} 
    */      
    loadView: function()
    {
        $("body").css("opacity","0");
        $('#home').html(this.view);
            
        animaVista(transitionReady);

        function transitionReady()
        {

            $('#btn_config').tap(function(){ 
                var controlador = new DatosUsuario();
                controlador.editarUsuario();
            });
            
            $('#grafica_peso').tap(function(){
                var controlador = new OperacionesPeso();
                var image1 = new Image();
                var image2 = new Image();
                var numCargadas=0;

                image1.onload = function()
                { 
                    numCargadas++;
                    cargaGrafica();
                };
                image2.onload = function()
                {
                    numCargadas++;
                    cargaGrafica();
                };
             

                image1.src = 'images/back_btn.png';
                image2.src = 'images/logoRotado.png';

                function cargaGrafica()
                {
                    if(numCargadas>1)
                    {
                        controlador.graficaPeso();
                    }
                    return false;
                }
            });
            
            $('#grafica_imc').tap(function(){
                var controlador = new OperacionesPeso();
                
                /** precarga de imágenes **/
                var image1 = new Image();
                var image2 = new Image();
                var image3 = new Image();
                var numCargadas=0;

                image1.onload = function()
                { 
                    numCargadas++;
                    cargaGrafica();
                };
                image2.onload = function()
                {
                    numCargadas++;
                    cargaGrafica();
                };
                image3.onload = function()
                {
                    numCargadas++;
                    cargaGrafica();
                };

                image1.src = 'images/back_btn.png';
                image2.src = 'images/logoRotado.png';
                image3.src = 'images/info.png';

                 /** termina  precarga de imágenes **/

                function cargaGrafica()
                {
                    if(numCargadas>2)
                    {
                        /**  Cuando se cargan las imágenes de llama la vista **/
                        controlador.graficaIMC();
                    }
                    return false;
                }



            });
            
            $('#meta_peso').tap(function(e){ 
                var controlador = new OperacionesPeso();
                controlador.registroMeta();
                return false;
            });
            
            $('#registrar_peso').tap(function(){ 
                var controlador = new OperacionesPeso();
                controlador.registroPeso();
            });
            
            $('#contador_calorias').tap(function(){ 
                var controlador = new Contenidos();
                controlador.listaCategorias();
               // suma1dia();
            });
            

            $('#tips').tap(function(){ 
                var controlador = new Contenidos();
                controlador.mostrarTips();
            });

            ///******* touchstate lista
            $('.lista_links li').on("touchstart", function(){

                $(this).css("background-color","lightgrey");
            });
            $('.lista_links li').on("touchmove", function(){

                $(this).css("background-color","transparent");
            });
            $('.lista_links li').on("touchend", function(){

                $(this).css("background-color","transparent");
            });
            ///*******
            
            $('#btn_config').on('touchstart', function(){
                $('#btn_config_img').attr('src', 'images/config_btn_pressed.png'); 
            });
                
            $('#btn_config').on('touchend', function(){
                $('#btn_config_img').attr('src', 'images/config_btn.png'); 
            });
                
            $('#btn_config').on('touchmove', function(){
                $('#btn_config_img').attr('src', 'images/config_btn.png'); 
            });
            
            $('#grafica_peso').on('touchstart', function(){
                $('#grafica_peso_img').attr('src', 'images/peso_btn_pressed.png'); 
            });
                
            $('#grafica_peso').on('touchend', function(){
                $('#grafica_peso_img').attr('src', 'images/peso_btn.png'); 
            });
                
            $('#grafica_peso').on('touchmove', function(){
                $('#grafica_peso_img').attr('src', 'images/peso_btn.png'); 
            });
            
            $('#grafica_imc').on('touchstart', function(){
                $('#grafica_imc_img').attr('src', 'images/imc_btn_pressed.png'); 

            });
                
            $('#grafica_imc').on('touchend', function(){
                $('#grafica_imc_img').attr('src', 'images/imc_btn.png'); 
            });
                
            $('#grafica_imc').on('touchmove', function(){
                $('#grafica_imc_img').attr('src', 'images/imc_btn.png'); 
            });
        }
        
        
    }
}