/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {TipSencillo}
 */
function TipSencillo(data)
{
    this.view="";
    this.view += "	<div class=\"logo_big\">";
    this.view += "    	<img src=\"images\/logo.png\" \/>";
    this.view += "<\/div>";
    this.view += "	<div class=\"content\">";
    this.view += "    	<div class=\"texto\" style=\"text-align:center\">"+data.tip.titulo+"<\/div>";
    this.view += "      <div class=\"texto_blanco\" id=\"texto_blanco\">";
    this.view += "          <ul>";
    this.view += "               <li><div class=\"contenedorImagenTip\"> <img src=\"images\/tips\/"+data.tip.nombreArchivo+"\"\/> </div><\/li>";
    this.view += "               <li><div class=\"contenedorTextoTip\">"+data.tip.texto+"</div><\/li>";
    this.view += "          <\/ul>";
    this.view += "      <\/div>";
    this.view += "    <\/div>";
    this.view += "    <div class=\"botones\">";
    this.view += "   	  <a href=#><div class=\"btn_izq\"><strong>Regresar<\/strong><\/div><\/a>";
    this.view += "    <\/div>";

}

TipSencillo.prototype = {
    /**
    * Bindings de botones.
    * @return {void} 
    */  
    loadView: function()
    {
        $("body").css("opacity","0");

        $('#home').html(this.view);

        animaVista(transitionReady);

        function transitionReady()
        {
            var myScroll = new iScroll('texto_blanco', { bounce: false, snap: 'li', momentum: false });

            $('div.btn_izq').tap(function(){
                myScroll.destroy();
                myScroll = null;
                var contenidos = new Contenidos();
                contenidos.mostrarTips();
            });
                
            $('div.btn_izq').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_izq').on('touchend', function(){
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_izq').on('touchmove', function(){
                $(this).removeClass('btn_pressed') 
            });
        }
    }
    
    }