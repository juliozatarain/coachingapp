/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {ListaTips}
 */
function ListaTips(data)
{
this.view="";
this.view += "	<div class=\"logo_big\">";
this.view += "    	<img src=\"images\/logo.png\" \/>";
this.view += "<\/div>";
this.view += "	<div class=\"content\">";
this.view += "    	<div class=\"texto\" style=\"text-align:center\">Nuestros tips<\/div>";
this.view += "       	<div class=\"forma\" style=\"text-align:right\">";
this.view += "<div id=\"wrapper\">";
this.view += "        	<ul class=\"lista_links scrollable\">";



if(data)
{
    for(var x = 0; x < data.length ; x++ )
    {
        this.view += "                 <li id=\""+data[x].id+"\" class=\"elementoMenu\">";
        this.view += "                      <div class=\"elementoCategLink\">";
        this.view += "                            <div class=\"nombreCateg\">";
        this.view +=                                data[x].titulo;
        this.view += "                            <\/div>";
        this.view += "                            <div class=\"flechaCateg\">";
        this.view += "                              <img src=\"images\/flecha.png\" \/>";
        this.view += "                            <\/div>";
        this.view += "                      <\/div>";
        this.view += "                  <\/li>";
    }
}
else
{
    this.view += "                 <li id=\"0\" class=\"elementoMenu\" style=\"pointer-events:none\"><a href=\"#\">";
    this.view += "                      <div class=\"elementoCategLink\">";
    this.view += "                          <div class=\"nombreCateg\">";
    this.view +=                                "No hay categorias.";
    this.view += "                            <\/div>";
    this.view += "                            <div class=\"flechaCateg\">";
    this.view += "                            <\/div>";
    this.view += "                      <\/div><\/a>";
    this.view += "                  <\/li>";
}

this.view += "			<\/ul>";
this.view += "<\/div>";
this.view += "        <\/div>";
this.view += "    <\/div>";
this.view += "    <div class=\"botones\">";
this.view += "   	  <a href=#><div class=\"btn_izq\"><strong>Regresar<\/strong><\/div><\/a>";
this.view += "    <\/div>";
this.view += "";

}

ListaTips.prototype =
{
    /**
    * Bindings de botones.
    * @return {void} 
    */  
    loadView: function()
    {
        $("body").css("opacity","0");

        $('#home').html(this.view);

        animaVista(transitionReady);

        function transitionReady()
        {
            $('div.nombreCateg').css('text-overflow','ellipsis');
            $('div.nombreCateg').css('white-space', 'nowrap');
            $('div.nombreCateg').css('width', '75%');
            $('div.nombreCateg').css('overflow', 'hidden');

            
            var myScroll = new iScroll('wrapper', { bounce: false, snap: 'li', momentum: false });

            $('div.btn_izq').tap(function(){
                myScroll.destroy();
                myScroll = null;
                var controlador = new DatosUsuario();
                controlador.menu();
            });
            
            $('li.elementoMenu').tap(function()
            {
                var contenidos = new Contenidos();
                contenidos.mostrarTip($(this).attr('id'));
            });
                
            $('div.btn_izq').on('touchstart', function(){
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_izq').on('touchend', function(){
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_izq').on('touchmove', function(){
                $(this).removeClass('btn_pressed') 
            });

             ///******* touchstate lista
            $('.lista_links.scrollable li').on("touchstart", function(){

                $(this).css("background-color","lightgrey");
            });
             $('.lista_links.scrollable li').on("touchmove", function(){

                $(this).css("background-color","transparent");
            });
              $('.lista_links.scrollable li').on("touchend", function(){

                $(this).css("background-color","transparent");
            });
            ///*******
        }
    }
}