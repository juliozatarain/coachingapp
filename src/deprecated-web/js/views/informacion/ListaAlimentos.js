/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {ListaAlimentos}
 */
function ListaAlimentos(data)
{
    this.datos=data.coleccion;
    this.view="";
    this.view += "	<div class=\"logo_big\">";
    this.view += "    	<img src=\"images\/logo.png\" \/>";
    this.view += "<\/div>";
    this.view += "	<div class=\"content\">";
    this.view += "    	<div class=\"texto\" style=\"text-align:center\">"+data.nombreCategoria+"<\/div>";
    this.view += "       	<div class=\"forma\" style=\"text-align:left\">";
    this.view += "<div class=\"contenedorBusquedaScroller\">";
    this.view += "  <div class=\"contenedorTextfieldBusqueda\">";
    this.view += "      <input type=\"text\" name=\"busqueda\" id=\"busqueda\" placeholder=\"Búsqueda\">";
    this.view += "      <div class=\"contenedorImagenBusqueda\">";
    this.view += "      <img height=\"22px\" src=\"images\/lupa.png\" \/>";
    this.view += "      <\/div>";
    this.view += "  <\/div>";
    this.view += "<div id=\"wrapperBusqueda\">";
    this.view += "        	<ul class=\"lista_links_busqueda\">";
                    for(var x = 0; x < data.coleccion.length ; x++ )
                    {
                        this.view += "<li>";
                            this.view += "<div class=\"elementoCateg\">";
                                this.view += "<div class=\"nombreCateg\">";
                                    this.view += data.coleccion[x].nombre;
                                this.view += "<\/div>";
                                this.view += "<div class=\"caloriasCateg\">";
                                    this.view += data.coleccion[x].kcal + "kcal";
                                this.view += "<\/div>";
                            this.view += "<\/div>";
                        this.view += "<\/li>";
                    }
    this.view += "			<\/ul>";
    this.view += "        <\/div>";
    this.view += "<\/div>";
    this.view += "    <\/div>";
    this.view += "    <\/div>";
    this.view += "    <div class=\"botones\">";
    this.view += "   	  <a href=#><div class=\"btn_izq\"><strong>Regresar<\/strong><\/div><\/a>";
    this.view += "    <\/div>";

}

ListaAlimentos.prototype = 
{
        /**
        * Bindings de botones, función de búsqueda.
        * @return {void} 
        */        
        loadView: function()
        {
            var obj=this.datos;

            $("body").css("opacity","0");

            $('#home').html(this.view);

            animaVista(transitionReady);

            function transitionReady()
            {
                var myScroll = new iScroll('wrapperBusqueda', { bounce: false, snap: 'li', momentum: false });
                // al salir de la vista se destruye el iScroll
                $('div.btn_izq').tap(function(){
                    myScroll.destroy();
                    myScroll = null;
                    var controlador = new Contenidos();
                    controlador.listaCategorias();
                });
                
                $('input#busqueda').keyup(function(event) 
                {
                    $('ul.lista_links_busqueda').empty();

                    reconstruyeBuscando($('#busqueda').val())

                }); 
                // Recorre el arreglo y recontruye la lista a partir del input del usuario
                function reconstruyeBuscando(value)
                {
                    var respuestas=false;
                    var cadenaHTML="";

                   for(var x = 0; x < obj.length ; x++)
                   {
                        if(obj[x].nombre.toLowerCase().indexOf(value.toLowerCase()) !== -1 )
                        {
                            respuestas=true;
                            cadenaHTML += "<li>";
                                cadenaHTML += "<div class=\"elementoCateg\">";
                                    cadenaHTML += "<div class=\"nombreCateg\">";
                                        cadenaHTML += obj[x].nombre;
                                    cadenaHTML += "<\/div>";
                                    cadenaHTML += "<div class=\"caloriasCateg\">";
                                        cadenaHTML += obj[x].kcal + "kcal";
                                    cadenaHTML += "<\/div>";
                                cadenaHTML += "<\/div>";
                            cadenaHTML += "<\/li>";
                        }
                   }


                   if(!respuestas)
                   {
                       cadenaHTML += "<li>";
                                cadenaHTML += "<div class=\"elementoCateg\">";
                                    cadenaHTML += "<div class=\"nombreCateg\">";
                                        cadenaHTML += "No hay coincidencias.";
                                    cadenaHTML += "<\/div>";
                                    cadenaHTML += "<div class=\"caloriasCateg\">";
                                    cadenaHTML += "<\/div>";
                                cadenaHTML += "<\/div>";
                            cadenaHTML += "<\/li>";
                   }     
                      $('ul.lista_links_busqueda').html(cadenaHTML);
                      //Se reconstruye el iScroll al rescotruir la lista de contenido
                      myScroll.refresh();
               
                }
                    
                $('div.btn_izq').on('touchstart', function(){
                    $(this).addClass('btn_pressed'); 
                });
                    
                $('div.btn_izq').on('touchend', function(){
                    $(this).removeClass('btn_pressed') 
                })
                    
                $('div.btn_izq').on('touchmove', function(){
                    $(this).removeClass('btn_pressed') 
                });

                

            }
        }
}