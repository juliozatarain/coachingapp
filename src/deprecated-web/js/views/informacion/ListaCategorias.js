/**
 * Se prepara la vista en html a partir de la informacion recibida en data
 * @constructor
 * @param {dictionary} data contiene la información para construir la vista
 * @this {ListaCategorias}
 */
function ListaCategorias(data)
{
    this.nombresCategorias=data;
    this.view="";
    this.view += "	<div class=\"logo_big\">";
    this.view += "    	<img src=\"images\/logo.png\" \/>";
    this.view += "<\/div>";
    this.view += "	<div class=\"content\">";
    this.view += "    	<div class=\"texto cat\" style=\"text-align:center\">Categor&iacute;as de alimentos";
    this.view += "      <br\/>por cada 100 gramos<\/div>";
    this.view += "       	<div class=\"forma\" style=\"text-align:right\">";


    this.view += "<div id=\"wrapper\">";
    this.view += "        	<ul class=\"lista_links scrollable\">";

    if(data)
    {
        for(var x = 0; x < data.length ; x++ )
        {
            this.view += "                 <li id=\""+data[x].id+"\" class=\"elementoMenu\">";
            this.view += "                      <div class=\"elementoCategLink\">";
            this.view += "                          <div class=\"nombreCateg\">";
            this.view +=                                data[x].nombre;
            this.view += "                            <\/div>";
            this.view += "                            <div class=\"flechaCateg\">";
            this.view += "                              <img src=\"images\/flecha.png\" \/>";
            this.view += "                            <\/div>";
            this.view += "                      <\/div>";
            this.view += "                  <\/li>";
        }
    }
    else
    {
        this.view += "                 <li id=\"0\" class=\"elementoMenu\" style=\"pointer-events:none\"><a href=\"#\">";
        this.view += "                      <div class=\"elementoCategLink\">";
        this.view += "                          <div class=\"nombreCateg\">";
        this.view +=                                "No hay categorias.";
        this.view += "                            <\/div>";
        this.view += "                            <div class=\"flechaCateg\">";
        this.view += "                            <\/div>";
        this.view += "                      <\/div><\/a>";
        this.view += "                  <\/li>";

    }

    this.view += "			<\/ul>";
    this.view += "<\/div>";
    this.view += "        <\/div>";
    this.view += "    <\/div>";
    this.view += "    <div class=\"botones\">";
    this.view += "   	  <a href=#><div class=\"btn_izq\"><strong>Regresar<\/strong><\/div><\/a>";
    this.view += "    <\/div>";
    this.view += "";

}

ListaCategorias.prototype =
{
    /**
    * Bindings de botones.
    * @return {void} 
    */  
    loadView: function()
    {
        $("body").css("opacity","0");

        $('#home').html(this.view);

        animaVista(transitionReady);

        var categorias = this.nombresCategorias;

        function transitionReady()
        {

            
            var myScroll = new iScroll('wrapper', { bounce: false, snap: 'li', momentum: false,  onScrollStart: function (e) {
                    e.preventDefault();

        } });

            $('div.btn_izq').tap(function()
            {
                myScroll.destroy();
                myScroll = null;
                var controlador = new DatosUsuario();
                controlador.menu();
            });
            
            $('li.elementoMenu').tap(function()
            {
                var contenidos = new Contenidos();
                // mandas el nombre de la categoria tambien, para imprimirlo en el titulo de la pagina que lista los alimentos
                contenidos.listaAlimentos($(this).attr('id'), categorias[$(this).attr('id') -1].nombre);
            });
                
            $('div.btn_izq').on('touchstart', function()
            {
                $(this).addClass('btn_pressed'); 
            });
                
            $('div.btn_izq').on('touchend', function()
            {
                $(this).removeClass('btn_pressed') 
            })
                
            $('div.btn_izq').on('touchmove', function()
            {
                $(this).removeClass('btn_pressed') 
            });
             ///******* touchstate lista
            $('.lista_links.scrollable li').on("touchstart", function(){

                $(this).css("background-color","lightgrey");
            });
             $('.lista_links.scrollable li').on("touchmove", function(){

                $(this).css("background-color","transparent");
            });
              $('.lista_links.scrollable li').on("touchend", function(){

                $(this).css("background-color","transparent");
            });
            ///*******
        }
    }
}