function GraficaPeso(data)
{
    this.data = data;
    this.view="";
    this.view+="<div class=\"contenedorExternoGrafica\">";
    this.view+="<div class=\"contenedorGrafica\">";
    this.view += "<div class=\"contenedorSombra\">";
    this.view += "<img src=\"images\/sombra.png\">";
    this.view += "<\/div>";  
    this.view += "<\/div>";  
    this.view += "<div class=\"contenedorBotonesYSombra\">";
    this.view += "<div class=\"botonesLogo\">";
    this.view += "<div class=\"contenedorBotonRegresar\">";
    this.view += "<img id=\"regresar\" src=\"images\/back_btn.png\">";
    this.view += "<\/div>";
    this.view += "<div class=\"contenedorTituloGrafica\">";
    this.view += "<div id=\"textoTitulo\">Mi Peso<\/div>";
    this.view += "<\/div>";
    this.view += "<div class=\"contenedorLogoPeso\">";
    this.view += "<img src=\"images\/logoRotado.png\">";
    this.view += "<\/div>";
    this.view += "<\/div>";  
    this.view += "<\/div>";
    this.view+="<\/div>";
}

GraficaPeso.prototype = 
{
    loadView: function()
    {

      var obj = this;

      $("body").css("opacity","0");

      $('#home').html(obj.view);

      animaVista(transitionReady);
      
      /**
        * Function: transitionReady
        * Se llama una vez que la vista este lista despues de una transicion.
        * Funcion principal de la vista.
        */
      function transitionReady()
      {
          
          //Binding de botones
            $('div.contenedorBotonRegresar').on("touchstart", function()
            {
                $("img#regresar").attr("src","images/back_btn_pressed.png");
            });
            $('div.contenedorBotonRegresar').on("touchend", function()
            {
                $("img#regresar").attr("src","images/back_btn.png");
            });
            $('div.contenedorBotonRegresar').on("touchmove", function()
            {
                $("img#regresar").attr("src","images/back_btn.png");
            });
            $('div.contenedorBotonRegresar').on("touchend", function()
            {
                var datosUsuario = new DatosUsuario();
                datosUsuario.menu();
            });

      
      /**
        * Obtiene el valor mayor de una lista de pesos.
        * 
        * @param {Array} this_array 
        * Arreglo del cual obtener el valor mayor.
        * 
        * @param {String} element
        * Elemento de comparacion del arreglo.
        * 
        * @return {Dict} maxValue
        * Elemento diccionario con peso y fecha.
        */
        function getMaxObjectValue(this_array, element) {
            var values = [];
            for (var i = 0; i < this_array.length; i++) {
                values.push(Math.ceil(parseFloat(this_array[i][""+element])));
            }
            values.sort(function(a,b){
                return a-b;
                });
            return values[values.length-1];
        }
 
        
       /**
        * Obtiene el valor menor de una lista de pesos.
        * 
        * @param {Array} this_array 
        * Arreglo del cual obtener el valor menor.
        * 
        * @param {String} element
        * Elemento de comparacion del arreglo.
        * 
        * @return {Dict} minValue
        * Elemento diccionario con peso y fecha.
        */
        function getMinObjectValue(this_array, element) {
            var values = [];
            for (var i = 0; i < this_array.length; i++) {
                values.push(Math.floor(parseFloat(this_array[i][""+element])));
            }
            values.sort(function(a,b){
                return a-b;
                });
            return values[0];
        }
 
 
        //Obtener datos y parsear fechas de cadena a formato fecha
        var parseDate = d3.time.format("%Y%m%d").parse;
            
        var data = obj.data.pesoReal;
        var dataConst = obj.data.pesoMeta;

        data.forEach(function(d) {
            d.fecha = parseDate(d.fecha);
        });
        
        
        //Datos para calcular tamanos y limites de vista
        var margin = {
            top: $(".contenedorGrafica").height() * 0.07, 
            right: $(".contenedorGrafica").height() * 0.06, 
            bottom: $(".contenedorGrafica").height() * 0.02, 
            left: $(".contenedorGrafica").height() * 0.05
            };

        var width = $(".contenedorGrafica").height()-margin.left-margin.right, height =$(".contenedorGrafica").width()- margin.top- margin.bottom;
 
        var minDate = (data[0].fecha),
        maxDate = data[data.length-1].fecha;
        minObjectValue = getMinObjectValue(data, 'peso');
        maxObjectValue = getMaxObjectValue(data, 'peso');
        if(dataConst<minObjectValue)
            minObjectValue=dataConst;
        if(dataConst>maxObjectValue)
            maxObjectValue=dataConst;
            
 
        //Crear el objeto e la grafica
        var vis= d3.select(".contenedorGrafica").append("svg")
        .data(data)
        .attr("class", "metrics-container")
        .attr("width", width +margin.left+ margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate("+(margin.left+height)+ "," + margin.top + ")")
        .append("g")
        .attr("transform", "rotate(90)");

    
        //Calcular dominios X y Y iniciales
        var fechaInicial =  minDate;
        var fechaFinal = new Date(minDate).setDate(minDate.getDate()+14);
        var y = d3.scale.linear().domain([ minObjectValue-(maxObjectValue - minObjectValue)/10, maxObjectValue+(maxObjectValue - minObjectValue)/7 ]).range([height, 0]),
        x = d3.time.scale().domain([fechaInicial, fechaFinal]).range([0, width]);
  
  
        //Calculo de escalas X y Y y formatos de fecha
        var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(5);
  
        var parseSemanas = d3.time.format("%W");
        var fechaMinEnSemana = parseSemanas(minDate);

        /**
        * Regresa el numero de semana. Funcion de arguento a d3.tickFormat para modificar los labels del axis X.
        * 
        * @param {String} cadenaFecha
        * Fecha a evaluar (en formato de cadena)
        * 
        * @param {Bool} sumar
        * True: Ajusta el valor al dominio cuando no se ha hecho ningun zoom.
        * False: No se ajusta la fecha.
        * 
        * @return {String} Cadena parseada en formato "Semana #"
        */
       function dameNumeroDeSemana(cadenaFecha, sumar)
        {
          var fecha =  parseSemanas(cadenaFecha) - fechaMinEnSemana;

          if(sumar)
            fecha  = Number(fecha) + Number(fechaMinEnSemana);


          if(fecha  == 0)
          {
            return  "";
          }
          else 
          {
            return "Semana "+fecha;
          }
        }


        /**
        * Regresa el dia de la semana. Funcion de arguento a d3.tickFormat para modificar los labels del axis X.
        * 
        * @param {String} cadenaFecha
        * Fecha a evaluar (en formato de cadena)
        * 
        * @param {Date} fechaTrans
        * Ajuste para que el valor del dominio corresponda a la fecha actual.
        * 
        * @return {String} Cadena parseada en formato "DiaSemana" (Lunes, jueves, etc.)
        */
        function dameDiaSemana(cadenaFecha,fechaTrans)
        {
                var parseDay = d3.time.format("%d/%m/%y");
                var fecha = parseDay(new Date(cadenaFecha.getTime()+fechaTrans.getTime()-367200000));
                return fecha;

        }
        
        /**
        * Regresa el mes del anio. Funcion de arguento a d3.tickFormat para modificar los labels del axis X.
        * 
        * @param {String} cadenaFecha
        * Fecha a evaluar (en formato de cadena)
        * 
        * @param {Date} fechaTrans
        * Ajuste para que el valor del dominio corresponda a la fecha actual.
        * 
        * @return {String} Cadena parseada en formato "Mes-anio"
        */
        function dameMes(cadenaFecha,fechaTrans)
        {
                var parseDay = d3.time.format("%b-%Y");
                var fecha =new Date(cadenaFecha.getTime()+fechaTrans.getTime()-367200000);
                fecha.setMonth(fecha.getMonth()+1);
                var mes = parseDay(fecha);
                return mes;

        }

        //Escala de X
        var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .ticks(d3.time.weeks, 1)
        .tickFormat(function(String){
            return dameNumeroDeSemana(String, false);
        });
    
 
        //Adicion del axis X
        vis.append("g")
        .attr("class", "x axis")
        .call(yAxis)
        .selectAll("path").attr("transform","scale(0.15,1)")

        
        //Ajustado a la pantalla
        var axisg = vis.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

        axisg.selectAll("path").attr("transform","scale(1,0.15)");


        //Creando objeto grid
        vis.append("g")         
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")

        //Contenedor de la grafica
        var svgs = vis.append("svg") .attr("width", width)
        .attr("width", width +margin.left+ margin.right)
        .attr("height", height + margin.top );

       //Dibujando linea del peso meta 
       svgs.append("svg:line")
        .attr("x1", 0)
        .attr("y1", y(dataConst))
        .attr("x2", width)
        .attr("y2", y(dataConst))
        .attr("class", "trazo2"); 


        //Nueva escala X (sera ajustada cuando se haga zoom)
        var x2 = d3.time.scale().domain([new Date(367200000), new Date(fechaFinal-fechaInicial+367200000)]).range([0, width]);
        xAxis.scale(x2)
              .orient('bottom')
              .ticks(d3.time.weeks,1)
              .tickFormat(function(String){
                  return dameNumeroDeSemana(String, true);
              });
        axisg.call(xAxis);
        
        
        var labelX; //Variable necesaria para la funcion redibuja, bandera interna.
        reDibuja(null);
        
        /**
        * Dibuja toda la grafica de acuerdo al dominio actual en X. Y nunca cambia.
        * 
        * @param {Number} interpola
        * Si interpola tiene un valor diferente null, los puntos de la grafica se interpolaran
        * cada cierto espacio de tiempo (1 por semana, 1 por dia, etc) indicado en milisegundos
        * por el valor de esta variable.
        * 
        * @return {String} Cadena parseada en formato "DiaSemana" (Lunes, jueves, etc.)
        */
        function reDibuja(interpola){
            var datos =  null;
            if(interpola == null) //Si interpola es null los datos son los mismos.
                datos = data;
            else    //Si interpola tiene un valor los datos se interpolan de acuerdo a este.
            {
                datos = [];
                var marca = data[0].fecha.getTime();
                var i=1;
                //Tomar el valor inicial.
                datos.push(data[0]);
                var actual = data[0].peso;
                var siguiente = data[0].peso;
                //Ciclar hasta la ultima fecha
                while(marca+interpola<data[data.length-1].fecha.getTime())
                {
                    //Avanzar en cada fecha de la lista hasta avanzar interpola milisegundos
                    while(data[i].fecha.getTime() < marca+interpola)
                    {
                        actual=data[i].peso;
                        i++;
                    }
                    //Si no se ha llegado al final, sacar el promedio de los dos valores antes y despues de la marca actual.
                    if(i<data.length-1)
                    {
                        siguiente = data[i+1].peso;
                        datos.push({'fecha': new Date(marca+interpola), 'peso': (Number(actual)+Number(siguiente))/2});
                    }
                    else{ //Si es el penultimo, tomar el su valor tal cual.
                        datos.push({'fecha': new Date(marca+interpola), 'peso': actual});
                    }
                    marca = marca+interpola;
                }
                //Tomar el ultimo valor
                datos.push(data[data.length-1]);
                
            }
            
            //Establecer modo de dibujo de las lineas y dominios
            var line = d3.svg.line()
                .interpolate("cardinal")
                .x(function(d) {
                    return x2(d["fecha"].getTime()+367200000-fechaInicial.getTime());
                })
                .y(function(d) {
                    return y(d["peso"]);
                })
            
            //Quitar lineas anteriores y dibujar nuevas lineas
            svgs.selectAll('path').remove();
            svgs.append("path")
                .attr("d", line(datos))
                .attr("class","trazo");
            
            //Quitar circulos y dibujar nuevos circulos
            svgs.selectAll('circle').remove();
            svgs.selectAll('circle')
                    .data(datos).enter()
                    .append('svg:circle')
                    .attr('class', 'dot')
                    .attr('style',"fill: green; stroke: black; stroke-width:2;")
                    .attr('cx', function(d) {
                        //Desplazamiento de acuerdo a la fecha del punto
                        var ret = x2(d.fecha.getTime()+367200000-fechaInicial.getTime());
                        if(ret<(width)/80 && ret>(width)/80)
                            return ret+(width)/80;
                        return ret;
                    })
                    .attr('cy', function(d) {return y(d.peso);})
                    .attr('r', function() {return width/80;});
            
            //Crear circulos invisibles para el evento touch
            svgs.selectAll('.touchArea')
                    .data(datos).enter()
                    .append('svg:circle')
                    .attr('class', 'touchArea')
                    .attr('style',"fill: rgba(0,0,0,0);")
                    .attr('cx', function(d) {
                        //Desplazamiento de acuerdo a la fecha del punto
                        var ret = x2(d.fecha.getTime()+367200000-fechaInicial.getTime());
                        if(ret<(width)/80 && ret>(width)/80)
                            return ret+(width)/80;
                        return ret;
                    })
                    .attr('cy', function(d) {return y(d.peso);})
                    .attr('r', function() {return width/18;})
                    .on("touchstart", function(d) {

                        //Colocar label cuando se hace touch en el punto
                        labelX=new Date(d.fecha.getTime()+367200000-fechaInicial.getTime());

                        svgs.selectAll('.label').remove();

                        svgs.selectAll('.label')
                            .data([d]).enter()
                            .append('text')
                            .attr('class','label')
                            .attr("x", function(d) {
                                var ret = x2(labelX)-(width)/80;
                                if(ret<(width)/70 && ret>-(width)/70){
                                    return ret+(width)/40;
                                }
                                return ret;
                            })
                            .attr("y", function(d) {return y(d.peso)-(width)/30;})
                            .attr("dx", 5)
                            .attr("style", "font-weight: bold; font-size: 16; text-anchor: middle;")
                            .text(Math.round(d.peso * 100) / 100)
                    });
                    
        }

        /**
        * Crea el axis Y
        * 
        * @return {d3.svg.axis} CaAxis de d3
        */
        function make_y_axis() {        
            return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(5)
        }

        //Eliminar lineas de los ticks.
        d3.selectAll('.tick.major').selectAll('line').remove()
        
        //Banderas para el zoom.
        var limZoom=0;  
        var interState = 0;
        var lastscale = 1;
        
        /**
        * Se encarga de capturar el evento zoom, reescalar los dominios y redibujar la grafica
        *  de acuerdo al zoom realizado.
        * 
        */
        var zoom = d3.behavior.zoom().scaleExtent([1/7,2.8]) //scaleExtent: Limites de zoomn
        .on("zoom",function() {
    
                //Eliminar zoom con doble click
              if(d3.event.sourceEvent.type=="touchstart" || d3.event.sourceEvent.type=="touchend"){
                  zoom.scale(lastscale);
                  return;
              }
              
              //Limites de escala y scroll
              lastscale=d3.event.scale;
              var s = false;
              var p = false;
              var num = (fechaFinal-fechaInicial)/(width);
              var maxSemana= ((maxDate-fechaInicial)*d3.event.scale/(num))-width;
              
              //Si se llega a los limites, anclar la escala y translacion.
              if(d3.event.translate[1]<-maxSemana){
                d3.event.translate[1]=-maxSemana;
                p=true;
              }
                  
              if(d3.event.translate[1]>0){
                d3.event.translate[1]=0;
                s=true;
              }
              if(s && p && d3.event.scale<1)
                if(limZoom==0)
                  limZoom=d3.event.scale;
                else
                  d3.event.scale=limZoom;
              else
                limZoom=0;
              
        
              //Calculo del nuevo dominio.
              var translado = 367200000-((d3.event.translate[1]*num)/d3.event.scale);
              var fechaIni=new Date(0+translado);
              var fechaFin=new Date(((fechaFinal-fechaInicial)/d3.event.scale)+translado);
              var interpola=null;
              x2 = d3.time.scale().domain([fechaIni, fechaFin]).range([0, width]);
              
              //Ajustar axis al nuevo dominio.
              xAxis.scale(x2)
              .orient('bottom')
              .ticks(d3.time.weeks,1)
              .tickFormat(function(String){
                  return dameNumeroDeSemana(String, true);
              });
              
              //Elegir el rango de interpolacion de los puntos.
              if(d3.event.scale>2.2){
			xAxis.ticks(d3.time.days,1).tickFormat(function(String){return dameDiaSemana(String,fechaInicial);});
              }
              if(d3.event.scale<0.64){
			xAxis.ticks(d3.time.weeks,1).tickFormat(function(String){return dameNumeroDeSemana(String, true);});
                        interpola = 3600000*56;
              }
              if(d3.event.scale<0.50){
			xAxis.ticks(d3.time.weeks,1).tickFormat(function(String){return dameNumeroDeSemana(String, true);});
                        interpola = 3600000*12*7;
              }
              if(d3.event.scale<0.38){
			xAxis.ticks(d3.time.months,1).tickFormat(function(String){return dameMes(String,fechaInicial);});
                        interpola = 3600000*24*7;
              }
              if(interpola == null){
                  if (interState != 0)
                      svgs.selectAll('.label').remove();
                  interState = 0;
              }
              else{
                  if(interpola == 3600000*56)
                  {
                    if (interState != 1)
                        svgs.selectAll('.label').remove();
                    interState = 1; 
                  }
                  else if(interpola == 3600000*12*7)
                  {
                    if (interState != 2)
                        svgs.selectAll('.label').remove();
                    interState = 2; 
                  }
                  else if(interpola == 3600000*24*7)
                  {
                    if (interState != 3)
                        svgs.selectAll('.label').remove();
                    interState = 3; 
                  }
                  
              }
              
              //Colocar nuevo axis.
              axisg.call(xAxis);
              
              //Desplazar label al nuevo escala/translado
              var ret = x2(labelX)-(width)/80;
              if(ret<(width)/70 && ret>-(width)/70){
                    ret= ret+(width)/40;
              }
              svgs.selectAll('.label')
                  .attr("x",ret);
              reDibuja(interpola); //Redibujar.
              
              //Eliminar lineas de los ticks.
              d3.selectAll('.tick.major').selectAll('line').remove();

            
        });




        //Bind del zoom al contenedor de la grafica.
        d3.select("svg").call(zoom)
        .on("dblclick.zoom", null);
      }
    }
}