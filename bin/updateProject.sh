#!/bin/sh

if test $1 = "android"
then
echo "Actualizando proyecto Android"
rm -rf ../src/android/assets/www/*
cp -rf ../src/deprecated-web/* ../src/android/assets/www/
cp ../src/cordovas/android/cordova-2.7.0.js ../src/android/assets/www/js/libs/
fi

if test $1 = "ios"
then
echo "Actualizando proyecto iOS"
rm -rf ../src/ios/www/*
cp -rf ../src/deprecated-web/* ../src/ios/www/
cp ../src/cordovas/ios/cordova-2.7.0.js ../src/ios/www/js/libs/
fi


if test $1 = "all"
then
echo "Actualizando proyecto Android"
rm -rf ../src/android/assets/www/*
cp -rf ../src/deprecated-web/* ../src/android/assets/www/
cp ../src/cordovas/android/cordova-2.7.0.js ../src/android/assets/www/js/libs/
echo "Actualizando proyecto iOS"
rm -rf ../src/ios/www/*
cp -rf ../src/deprecated-web/* ../src/ios/www/
cp ../src/cordovas/ios/cordova-2.7.0.js ../src/ios/www/js/libs/
fi